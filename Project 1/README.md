# Thomas Fowler
# CST-310, MWF155A
# Jeff Griffith
# 16 January 2018

# README #

### What is this repository for? ###

* This is for the Sierpinski Gasket Project for Computer Graphics
* Version 1.0

### How do I get set up? ###

In order to run these programs:
1) Extract the "Sierpinski Gasket (Fowler).zip" file.
2) The Sierpinski Gasket folder within this newly extracted folder will contain the following files: firstGasket.html, 
   firstGasket.js, secondGasket.html, secondGasket.js, and thirdGasket.html.
3) To view the output of each of these, simply double click the .html files and it will open up your default HTML5 
   compatible browser that can view the Sierpinski Gaskets.

### Notes ###

* I used Visual Studio Code on macOS for editing the .html and .js files, and I double clicked the .html files to run them.