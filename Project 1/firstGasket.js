// Thomas Fowler
// CST-310, MWF155A
// Jeff Griffith
// 16 January 2018
// This code is based off the code provided from chapter 2 in the
// textbook.

// Used for error handling 
"use strict";

// Initialize variables
var gl;
var canvas;
var points = [];
var subdivisions = 6;

// The onload event is the function that begins the program. It is set to an
// address of a function this means that function will execute first before 
// any other JavaScript code.
window.onload = function init()
{
    // Retrieve the canvas and call our general purpose 
    // chrome provided routine for setting up a WebGL canvas.
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );

    // Return an error if unable to retrieve.
    if ( !gl ) { alert( "WebGL isn't available" ); }

    // Initialize each of the three corners of the Gasket
    var vertices = [
        vec2( -1, -1 ),
        vec2(  0,  1 ),
        vec2(  1, -1 )
    ];

    // Call the partitionTriangle function declared after init()
    partitionTriangle( vertices[0], vertices[1], vertices[2],
                    subdivisions);

    // These lines configure WebGL by specifying the window coordinates and the
    // background color.
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 1.0, 1.0, 1.0 );

    // Load the shaders from the HTML script and initialize attribute buffers.
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU. bindBuffer is a WebGL function that accepts WebGL parameters.
    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer. vPosition was defined in the HTML script
    // as an attribute, gl.getAttribLocation adds the WebGL context. 
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    // Render essentially fetches the data
    render();
};

// The triangle function simply pushes the points into the array.
// It is used in the next function, partitionTriangle.
function triangle( a, b, c )
{
    points.push( a, b, c );
}

// The partitionTriangle function takes in parameters for all three points that
// make up the triangle, and performs recursive operations to come up with the
// Serpinski gasket.
function partitionTriangle( a, b, c, count )
{
    // Check for the end of recursion
    if ( count === 0 ) 
    {
        triangle( a, b, c );
    }
    else 
    {
        // Cut each of the sides in half
        var ab = mix( a, b, 0.5 );
        var ac = mix( a, c, 0.5 );
        var bc = mix( b, c, 0.5 );

        // Decrement the count parameter
        --count;

        // Recursively call the function with three new triangles
        partitionTriangle( a, ab, ac, count );
        partitionTriangle( c, ac, bc, count );
        partitionTriangle( b, bc, ab, count );
    }
}

function render()
{
    // Clear the framebuffer
    gl.clear( gl.COLOR_BUFFER_BIT );

    // Render the vertex data that is on the GPU.
    gl.drawArrays( gl.TRIANGLES, 0, points.length );
}