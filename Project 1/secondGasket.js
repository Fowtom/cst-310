// Thomas Fowler
// CST-310, MWF155A
// Jeff Griffith
// 16 January 2018
// This code is based off the code provided from chapter 2 in the
// textbook.

// Used for error handling.
"use strict";

// Initialize variables
var canvas;
var gl;
var points = [];
var colors = [];
var subdivisions = 4;

// The onload event is the function that begins the program. It is set to an
// address of a function this means that function will execute first before 
// any other JavaScript code.
window.onload = function init()
{
    // Retrieve the canvas and call our general purpose 
    // chrome provided routine for setting up a WebGL canvas.
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );

    // Return an error if unable to retrieve.
    if ( !gl ) { alert( "WebGL isn't available" ); }

    // Start off by initializing the four, 3D vertices of the gasket.
    var vertices = [
        vec3(  0.0000,  0.0000, -1.0000 ),
        vec3(  0.0000,  0.9428,  0.3333 ),
        vec3( -0.8165, -0.4714,  0.3333 ),
        vec3(  0.8165, -0.4714,  0.3333 )
    ];

    // Call the divideTetra function declared after init()
    divideTetra( vertices[0], vertices[1], vertices[2], vertices[3],
                 subdivisions);

    // These lines configure WebGL by specifying the window coordinates and the
    // background color.
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // This allows for hidden surfaces to be removed
    gl.enable(gl.DEPTH_TEST);

    // Load the shaders from the HTML script and initialize attribute buffers.
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU. bindBuffer is a WebGL function that accepts WebGL parameters.
    // This essentially creates the buffer object, initializes it, and then associates it with
    // the attribute variable in the vertex shader.
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer. vColor was defined in the HTML script
    // as an attribute, gl.getAttribLocation adds the WebGL context. 
    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    // Load the data into the GPU. This essentially creates the buffer object, initializes it, 
    // and then associates it with the attribute variable in the vertex shader.
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer. vPosition was defined in the HTML script
    // as an attribute, gl.getAttribLocation adds the WebGL context. 
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    // Fetch the data
    render();
};

// This function pushes the points into the array, as well as change each of the colors according
// to the base color array. It is used in the next function, tetra.
function triangle( a, b, c, color )
{

    // Add the colors and vertices for a triangle.
    var baseColors = [
        vec3(1.0, 0.0, 1.0),
        vec3(0.0, 1.0, 0.0),
        vec3(0.0, 1.0, 1.0),
        vec3(1.0, 1.0, 0.0)
    ];

    // Push the data to respective arrays.
    colors.push( baseColors[color] );
    points.push( a );
    colors.push( baseColors[color] );
    points.push( b );
    colors.push( baseColors[color] );
    points.push( c );
}

// This function calls the triangle function four times, each with a different color.
function tetra( a, b, c, d )
{
    triangle( a, c, b, 0 );
    triangle( a, c, d, 1 );
    triangle( a, b, d, 2 );
    triangle( b, c, d, 3 );
}

// This function takes in parameters for each of the four tetrahedronal vertices, as well
// as the count for how many times to recursively subdivide them.
function divideTetra( a, b, c, d, count )
{
    // Check for the end of recursion.
    if ( count === 0 ) 
    {
        tetra( a, b, c, d );
    }

    else 
    {
        // Cut each of the sides in half
        var ab = mix( a, b, 0.5 );
        var ac = mix( a, c, 0.5 );
        var ad = mix( a, d, 0.5 );
        var bc = mix( b, c, 0.5 );
        var bd = mix( b, d, 0.5 );
        var cd = mix( c, d, 0.5 );
        
        // Decrement the count parameter
        --count;

        // Recursively call the function with four smaller tetrahedra.
        divideTetra(  a, ab, ac, ad, count );
        divideTetra( ab,  b, bc, bd, count );
        divideTetra( ac, bc,  c, cd, count );
        divideTetra( ad, bd, cd,  d, count );
    }
}

function render()
{
    // Clear the framebuffer
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Render the vertex data that is on the GPU.
    gl.drawArrays( gl.TRIANGLES, 0, points.length );
}
