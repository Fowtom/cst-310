class BezierWing {
    constructor(color,xAngle,yAngle,zAngle,xtrans,ytrans,ztrans) {
        // Predefined control points from the Project #9 save file.
        var wingControlPoints = [-50,-50,0,-25,50,0,25,50,0,50,-50,0,-50,-50,0];
        var wingControlPointsZ = [-50,-50,10,-25,50,10,25,50,10,50,-50,10,-50,-50,10];

        // Used to store the data
        var thePoints = [];
        var indexPoints = [];
        var xShapePoints = [];
        var yShapePoints = [];
        var zShapePoints = [];
        var xShapeControlPoints = [];
        var yShapeControlPoints = [];
        var zShapeControlPoints = [];

        // Call the function to get all of the vertices
        getShapeControlPoints(wingControlPoints);
        getShapeControlPoints(wingControlPointsZ);

        function getShapeControlPoints(array){
            var xPoints = [];
            var yPoints = [];
            var zPoints = [];
            // Iterate through the mouse points array
            for(var i = 0; i < array.length; i++) {
                // If the last parameter of the point has been reached
                if(i%3 == 0) { xPoints.push(array[i]); }
                else if(i%3 == 1) { yPoints.push(array[i]); }
                else if(i%3 == 2) { zPoints.push(array[i]); }
            }
            // Push all the control points for later use
            xShapeControlPoints.push([xPoints[0],xPoints[1],xPoints[2],xPoints[3],xPoints[4]]);
            yShapeControlPoints.push([yPoints[0],yPoints[1],yPoints[2],yPoints[3],yPoints[4]]);
            zShapeControlPoints.push([zPoints[0],zPoints[1],zPoints[2],zPoints[3],zPoints[4]]);

            generateShape();
        }
        
        function generateShape(){
            // Used to represent how many curves will be on the canvas based on the control points
            var curveCount = xShapeControlPoints.length;
            // Iterate through all of the control points
            for(var j = 0; j < curveCount; j++) {
                // Iterate through i (100 vertices from point A to point B)
                for(var i = 0; i < 1.0; i += 0.01) {
                    // Calculate the x and y for a given Bezier vertex
                    var x = Bezier(i, xShapeControlPoints[j]);
                    var y = Bezier(i, yShapeControlPoints[j]);
                    var z = Bezier(i, zShapeControlPoints[j]);
                    // Push it to the vertices array
                    xShapePoints.push(x);
                    yShapePoints.push(y);
                    zShapePoints.push(z);
                }
            }

            //xShapeControlPoints.length = yShapeControlPoints.length = zShapeControlPoints.length = 0;
            xShapeControlPoints = [];
            yShapeControlPoints = [];
            zShapeControlPoints = [];
        }

        // Pushing all the points as a vec4 into thePoints
        for(var i = 0; i < 200; i++){
            var x = xShapePoints[i];
            var y = yShapePoints[i];
            var z = zShapePoints[i];

            var initPoint = vec4(x,y,z,1);

            initPoint = mult(mat4(initPoint), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
            initPoint = mult(mat4(initPoint), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
            initPoint = mult(mat4(initPoint), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
            initPoint[0] += xtrans; initPoint[1] += ytrans; initPoint[2] += ztrans; // Handle translation
            thePoints.push(initPoint);
        }

        // Add faces to the edge of the shape
        for(var i = 0; i < 99; i++){
            indexPoints.push(thePoints[101+i]);
            indexPoints.push(thePoints[1+i]);
            indexPoints.push(thePoints[i]);
            indexPoints.push(thePoints[100+i]);
            pushPoints();
        }

        // Add faces to the top of the shape
        for(var i = 0; i < 100; i++){
            indexPoints.push(thePoints[101-i]);
            indexPoints.push(thePoints[1+i]);
            indexPoints.push(thePoints[i]);
            indexPoints.push(thePoints[100-i]);
            pushPoints();
        }

        // Add faces to the bottom of the shape
        for(var i = 0; i < 99; i++){
            indexPoints.push(thePoints[199-i]);
            indexPoints.push(thePoints[101+i]);
            indexPoints.push(thePoints[100+i]);
            indexPoints.push(thePoints[198-i]);
            pushPoints();
        }
        
        function pushPoints() {
            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);
            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {
                // Add to array each triangle
                points.push( indexPoints[indices[i]] );
                normalsArray.push(normal);
                // Color the two triangles
                colors.push(faceColors[color]);
            }
            indexPoints = [];          // Reset indexPoints to handle the next four vertices
        }
    }
}

