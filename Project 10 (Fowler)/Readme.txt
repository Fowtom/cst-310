Thomas Fowler
CST-310, MWF155A
Jeff Griffith
24 April 2018

PROJECT 10: ADVANCED CURVES AND SURFACES

In this project, I used my project #9 implementation to create a set of control points that are used to generate a 3D bezier curve object. This object follows a Bezier path.

DIRECTIONS TO RUN THE PROGRAM
1) Ensure that project10.html, project10.js, bezierWing.js, cylinder.js, sphere.js, and airplane.js are all in the same directory, and the folder that contains both of these is in the same directory as the "Common" folder with all of the extra JavaScript files.
2) Open "project10.html"
3) If executed correctly, an airplane that moves along a path should show up in the canvas. The buttons can be pressed for rotation and scaling.