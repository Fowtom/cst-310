"use strict";

// Initialize canvas and gl variables
var canvas;
var gl;

// Used to store the data
var points = [];
var normalsArray = [];
var colors = [];

var xPathPoints = [];
var yPathPoints = [];
var zPathPoints = [];

var xControlPoints = [];
var yControlPoints = [];
var zControlPoints = [];

// Used for positioning with the translation matrix
var pathCounter = 0;
var xPoint;
var yPoint;
var zPoint;
var xPointLoc;
var yPointLoc;
var zPointLoc;

// Defines the control points of the path
var path = [0,0,-150,
            -50,150,0,
            150,-150,150,
            100,0,300,
            -150,150,150,
            150,-150,0,
            0,0,-150];

// Used for Bezier polynomial function
var interp = [[1],
            [1,1],
            [1,2,1],
            [1,3,3,1],
            [1,4,6,4,1],
            [1,5,10,10,5,1],
            [1,6,15,20,15,6,1]];

// Used for rotation and scaling
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];
var thetaLoc;
var scale = 0.01;
var scaleLoc;
var translation = 0;
var transLoc;

// Used for camera positioning
var near = 1;
var far = 100;
var fovy = 45.0;
var aspect;
var flag = false;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;
var eyePt = vec3(0, 0, 5);
const atPt = vec3(0.0, 0.0, -100);
const upVec = vec3(0.0, 1.0, 0.0);

// These variables handle the light aspects
var lightPosition = vec4(0.0, 10.5, 200.0, 0.0 );
var materialShininess = 100.0;
var lightAmbient = vec4(0.1, 0.1, 0.1, 1.0 );
var lightDiffuse = vec4( .5, .5, .5, 1.0 );
var lightSpecular = vec4( .5, .5, .5, 1.0 );
var materialAmbient = vec4( .1, .1, .1, 1.0 );
var materialDiffuse = vec4( 1, 1, 1, 1.0);
var materialSpecular = vec4( .1, .1, .1, 1.0 );

// Used for loading the shaders
var program;

// Array to hold the colors
var faceColors = [[ 0.68, 0.68, 0.68, 1.0 ]];  // gray

window.onload = function init() {
    
    // Get the canvas and shaders from the HTML
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    program  = initShaders( gl, "vertex-shader", "fragment-shader" );

    // Create the airplane
    var airplane = new Airplane(0,0,0,0);

    // Specify canvas properties
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.725, 1.0, 0.78, 1.0 );
    gl.enable(gl.DEPTH_TEST);     

    //  Compile, Link shaders into program
    gl.useProgram( program );

    // Call these functions to create and specify the buffers
	loadVertices(program);
    loadColors(program);

    // Get the control points for the path of the object
    getPathControlPoints(path);

    // Get the location of the angle, scale, translations, and points
    thetaLoc = gl.getUniformLocation(program, "theta");
    scaleLoc = gl.getUniformLocation(program, "scale");
    transLoc = gl.getUniformLocation(program, "translation");
    xPointLoc = gl.getUniformLocation(program, "xPoint");
    yPointLoc = gl.getUniformLocation(program, "yPoint");
    zPointLoc = gl.getUniformLocation(program, "zPoint");

    // Buttons
    document.getElementById( "xButton" ).onclick = function () { axis = xAxis; flag = true; };
    document.getElementById( "yButton" ).onclick = function () { axis = yAxis; flag = true; };
    document.getElementById( "zButton" ).onclick = function () { axis = zAxis; flag = true; };
    document.getElementById( "scaleUpButton" ).onclick = function () { scale += 0.005; };
    document.getElementById( "scaleDownButton" ).onclick = function () { scale -= 0.005; };

    setViewProjection(program); // Call this to handle camera positioning using ViewMatrix and ProjectionMatrix
    render();                   // Render the scene
}

function loadVertices(program) {

    // Load normals for the diffuse light source
    var nBuffer = gl.createBuffer();                                         // Store normal vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normalsArray), gl.STATIC_DRAW ); // Vertices stored in normalsArray.
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );            // Gets variable from vertex shader
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );           // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vs_Normal );                                 // Enables the vertex attribute by copying the buffer to the VBO of WebGL

    var vBuffer = gl.createBuffer();                                         // Store vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );       // Vertices stored in points
    
    // Load the vertices for the triangles and enable the attribute vPosition
    var vPosition = gl.getAttribLocation( program, "vPosition" );            // Gets variable from vertex shader 
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );           // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vPosition );                                 // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadColors(program) {

    // Store colors in a buffer
    var cBuffer = gl.createBuffer();                                         // Store color vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );       // Vertices stored in colors

    var vColor = gl.getAttribLocation( program, "vColor" );                  // Load the colors for the triangles and enable the attribute vColor
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );              // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vColor );                                    // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function setViewProjection( program ){

    VMatrixLoc = gl.getUniformLocation( program, "vs_ViewMatrix" );          // Gets the location of the view matrix
    PMatrixLoc = gl.getUniformLocation( program, "vs_ProjMatrix" );          // Gets the location of the projection matrix

    // Return error message if the location cannot be found
    if(!VMatrixLoc || !PMatrixLoc) { console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix'); return; }

    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                              // Set the view matrix depending on the camera coordinates
    aspect = canvas.width/canvas.height;                                     // Set the aspect to ratio of canvas width and height
    vs_ProjMatrix = perspective(fovy, aspect, near, far);                    // Set the projection matrix depending on the camera coordinates

    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix));         // Set up the uniform matrix for the view matrix
    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix));         // Set up the uniform matrix for the projection matrix
}

function loadPhongModel() {
    var ambientProduct = mult(lightAmbient, materialAmbient);                // Calculate the ambientProduct
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);                // Calculate the diffuseProduct
    var specularProduct = mult(lightSpecular, materialSpecular);             // Calculate the specularProduct

    gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"), flatten(ambientProduct) );   // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"), flatten(diffuseProduct) );   // Set up the uniform matrix for the diffuse
    gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"), flatten(specularProduct) ); // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"), flatten(lightPosition) );     // Set up the uniform matrix for the light position
    gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"), materialShininess);                // Set up the uniform matrix for the shininess
}

function binomial(n,k) {
    // Iterate through the length of the higher order polynomial coefficients
    while(n >= interp.length) {
        // Define relevant variables for calculation
        var s = interp.length;
        var prev = s - 1;
        var nextRow = [];
        // Since each iteration starts with 1
        nextRow.push(1);
        // Calculate the next value and push it based on previous iteration
        for(var i = 0; i < prev; i++) {
            nextRow.push(interp[prev][i-1]+interp[prev][i]);
        }
        // Since each iteration ends with 1
        nextRow.push(1);
        // Push the next row into the array
        interp.push(nextRow);
    }
    // Return the resulting polynomial value
    return interp[n][k];
}

function Bezier(t,w){
    // Used for Bezier calculation
    var n = w.length-1;
    var sum = 0;
    // Iterate through parameter length and calculate the sum
    for (var k = 0; k < w.length; k++) {
        sum += w[k] * binomial(n,k) * Math.pow((1-t),(n-k)) * Math.pow(t,k);
    }
    return sum;
}

function generatePath(){
    // Used to represent how many curves will be on the canvas based on the control points
    var curveCount = xControlPoints.length;
    // Iterate through all of the control points
    for(var j = 0; j < curveCount; j++) {
        // Iterate through i (100 vertices from point A to point B)
        for(var i = 0; i < 1.0; i += 0.01) {
            // Calculate the x and y for a given Bezier vertex
            var x = Bezier(i, xControlPoints[j]);
            var y = Bezier(i, yControlPoints[j]);
            var z = Bezier(i, zControlPoints[j]);
            // Push it to the vertices array
            //pathPoints.push(vec4(x, y, z, 1));
            xPathPoints.push(x);
            yPathPoints.push(y);
            zPathPoints.push(z);
        }
    }
}

function getPathControlPoints(array){
    var xPoints = [];
    var yPoints = [];
    var zPoints = [];
    // Iterate through the mouse points array
    for(var i = 0; i < array.length; i++) {
        // If the last parameter of the point has been reached
        if(i%3 == 0) { xPoints.push(array[i]); }
        else if(i%3 == 1) { yPoints.push(array[i]); }
        else if(i%3 == 2) { zPoints.push(array[i]); }
    }
    xControlPoints.push([xPoints[0],xPoints[1],xPoints[2],xPoints[3],xPoints[4],xPoints[5],xPoints[6]]);
    yControlPoints.push([yPoints[0],yPoints[1],yPoints[2],yPoints[3],yPoints[4],yPoints[5],yPoints[6]]);
    zControlPoints.push([zPoints[0],zPoints[1],zPoints[2],zPoints[3],zPoints[4],zPoints[5],zPoints[6]]);

    generatePath();
}

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );            // Create the window on the screen

    if(flag) theta[axis] += 1.0;                                      // If a rotate or freeze button has been pressed, react accordingly
    gl.uniform3fv(thetaLoc, theta);                                   // Account for the location of theta
    gl.uniform1f(scaleLoc, scale);                                    // Account for the location of scale
    gl.uniform1f(transLoc, translation);                              // Account for the location of translation
    gl.uniform1f(xPointLoc, xPoint);                                  // Account for the location of xPoint
    gl.uniform1f(yPointLoc, yPoint);                                  // Account for the location of yPoint
    gl.uniform1f(zPointLoc, zPoint);                                  // Account for the location of zPoint

    // If the end of the path has not been reached, set the current x, y, and z point
    if(pathCounter < xPathPoints.length){
        xPoint = xPathPoints[pathCounter];
        yPoint = yPathPoints[pathCounter];
        zPoint = zPathPoints[pathCounter];
        pathCounter++;
    }
    // If the end of the path has been reached, reinitialize the counter and set the current x, y, and z point
    else{
        pathCounter = 0;
        xPoint = xPathPoints[pathCounter];
        yPoint = yPathPoints[pathCounter];
        zPoint = zPathPoints[pathCounter];
        pathCounter++;
    }

    vs_ProjMatrix = perspective(fovy, aspect, near, far);                 // Set the position of the projection matrix
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                           // Set the position of the view matrix

    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0])); // Handle rotation in the x-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0])); // Handle rotation in the y-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1])); // Handle rotation in the z-direction

    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix) );     // Set up the uniform matrix for the projection matrix
    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix) );     // Set up the uniform matrix for the view matrix
    loadPhongModel();                                                     // Call loadPhongModel()
    gl.drawArrays( gl.TRIANGLES, 0, points.length );                      // Use gl.TRIANGLES to draw everything

    requestAnimFrame( render );                                           // Call the render function recursively for each frame
}