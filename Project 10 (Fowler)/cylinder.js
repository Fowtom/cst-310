class Cylinder {
    constructor(iterations,height,radius,color,xAngle,yAngle,zAngle,xtrans,ytrans,ztrans) {
        // Initialize arrays
        var thePoints = [];
        var indexPoints = [];

        // Loop through for each degree of the circle top and base
        for(var i = 0; i < iterations+1; i++) {

            var theta = i * 2 * Math.PI/iterations;
            // Top circle
            var x1 = radius * Math.cos(theta);
            var y1 = radius * Math.sin(theta);
            var z1 = height;
            // Base circle
            var x2 = radius * Math.cos(theta);
            var y2 = radius * Math.sin(theta);
            var z2 = 0;

            var a = vec4(x1,y1,z1,1.0);         
            var b = vec4(x2,y2,z2,1.0);
            a = mult(mat4(a), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
            b = mult(mat4(b), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
            a = mult(mat4(a), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
            b = mult(mat4(b), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
            a = mult(mat4(a), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
            b = mult(mat4(b), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
            a[0] += xtrans; a[1] += ytrans; a[2] += ztrans;
            b[0] += xtrans; b[1] += ytrans; b[2] += ztrans;
            thePoints.push(a);
            thePoints.push(b);
        }

        // This loop takes the vertices that make up where the two triangles will go and pushes them to
        // a temporary array that will be used in determining the normal, color, point, and texture vertices.
        for(var i = 0; i < iterations*2; i+=2) {
            indexPoints.push(thePoints[i]);     // Vertex 0
            indexPoints.push(thePoints[i+1]);   // Vertex 1
            indexPoints.push(thePoints[i+3]);   // Vertex 2
            indexPoints.push(thePoints[i+2]);   // Vertex 3
            pushPoints();
        }

        // Build cylinder sides
        for(var i = 0; i < iterations*2; i+=2) {
            indexPoints.push(thePoints[i]);     // Vertex 0
            indexPoints.push(thePoints[i+1]);   // Vertex 1
            indexPoints.push(thePoints[iterations*2-i]);   // Vertex 2
            indexPoints.push(thePoints[iterations*2-1-i]);   // Vertex 3
            pushPoints();
        }

        function pushPoints() {

            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points.push( indexPoints[indices[i]] );
                normalsArray.push(normal);

                // Color the two triangles
                colors.push(faceColors[color]);

            }
            indexPoints = [];
        }
    }
}