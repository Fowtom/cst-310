class Airplane {
    constructor(color,xtrans,ytrans,ztrans) {
        // Create the airplane using cylinders, spheres, and the bezier wings.
        var cylCenter = new Cylinder(50,100,20,0,0,0,0,0+xtrans,0+ytrans,0+ztrans);
        var sphEnd1 = new Sphere(50,20,0,0+xtrans,0+ytrans,100+ztrans);
        var sphEnd2 = new Sphere(50,20,0,0+xtrans,0+ytrans,0+ztrans);
        var wing1 = new BezierWing(0,90,45,0,85+xtrans,0+ytrans,40+ztrans);
        var wing2 = new BezierWing(0,270,-45,180,-85+xtrans,0+ytrans,40+ztrans);
        var wheel1 = new Cylinder(50,5,5,0,0,90,0,-10+xtrans,-20+ytrans,20+ztrans);
        var wheel2 = new Cylinder(50,5,5,0,0,90,0,-10+xtrans,-20+ytrans,70+ztrans);
        var wheel3 = new Cylinder(50,5,5,0,0,90,0,15+xtrans,-20+ytrans,70+ztrans);
        var wheel3 = new Cylinder(50,5,5,0,0,90,0,15+xtrans,-20+ytrans,20+ztrans);
    }
}