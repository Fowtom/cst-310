Thomas Fowler
CST-310, MWF155A
Jeff Griffith
16 April 2018

PROJECT 9: ADVANCED CURVES AND SURFACES

In this project, I used Bezier curves to create an interactive canvas. The vertices can be saved and loaded for later use.

DIRECTIONS TO RUN THE PROGRAM
1) Ensure that bezier.html, bezier.js, and writeFile.js are all in the same directory, and the folder that contains both of these is in the same directory as the "Common" folder with all of the extra JavaScript files.
2) Open "bezier.html"
3) If executed correctly, select four points on the canvas to draw the Bezier curve. A new curve will be drawn for every four points the vertex array is given.