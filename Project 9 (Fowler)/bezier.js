// Tom Fowler
// CST-310, MWF155A
// Jeff Griffith
// 16 April 2018
// Project 9: Advanced Curves and Surfaces

"use strict";

// Used to store the data
var vertices = [];
var xMousePoints = [];
var yMousePoints = [];
var xControlPoints = [];
var yControlPoints = [];

// Used to keep track of how many vertices are in the buffer
var numVertices = 0;

// Important variables for setting everything up
var canvas;
var gl;
var program;
var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;

// Used for the camera
var eye = vec3(0.0, 0.0, 5.0);
var at = vec3(0.0, -0.05, -35);
var up = vec3(0.0, 1.0, 0.0);
var near = 1;
var far = 100;
var fovy = 49.0;
var aspect = 1.0;
var scale = 5;
var eye_y = 0.025;

// Used for Bezier polynomial function
var interp = [[1],
            [1,1],
            [1,2,1],
            [1,3,3,1],
            [1,4,6,4,1],
            [1,5,10,10,5,1],
            [1,6,15,20,15,6,1]];

function binomial(n,k) {
    // Iterate through the length of the higher order polynomial coefficients
    while(n >= interp.length) {
        // Define relevant variables for calculation
        var s = interp.length;
        var prev = s - 1;
        var nextRow = [];
        // Since each iteration starts with 1
        nextRow.push(1);
        // Calculate the next value and push it based on previous iteration
        for(var i = 0; i < prev; i++) {
            nextRow.push(interp[prev][i-1]+interp[prev][i]);
        }
        // Since each iteration ends with 1
        nextRow.push(1);
        // Push the next row into the array
        interp.push(nextRow);
    }
    // Return the resulting polynomial value
    return interp[n][k];
}

function Bezier(t,w){
    // Used for Bezier calculation
    var n = w.length-1;
    var sum = 0;
    // Iterate through parameter length and calculate the sum
    for (var k = 0; k < w.length; k++) {
        sum += w[k] * binomial(n,k) * Math.pow((1-t),(n-k)) * Math.pow(t,k);
    }
    return sum;
}


function generateBCurve(){
    // Used to represent how many curves will be on the canvas based on the control points
    var curveCount = xControlPoints.length;
    // Iterate through all of the control points
    for(var j = 0; j < curveCount; j++) {
        // Iterate through i (100 vertices from point A to point B)
        for(var i = 0; i < 1.0; i += 0.01) {
            // Calculate the x and y for a given Bezier vertex
            var x = Bezier(i, xControlPoints[j])/scale;
            var y = Bezier(i, yControlPoints[j])/scale;
            // Push it to the vertices array
            vertices.push(vec4(x, y, 0, 1));
            // Increment numVertices
            numVertices += 1;
        }
    }
}

window.onload = function init() 
{
    // Specify canvas properties
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 1.0, 1.0, 1.0 );
    // Initialize the program
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
    // Find the modelView and projection matrices locations
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
    // Whenever the user clicks on the canvas
    canvas.addEventListener("click", function() {
        // Set the x and the y positions for the position the canvas was clicked
        var x = event.offsetX;
        var y = event.offsetY;
        // Modify these x and y values so they will display on the canvas
        x = (12 * ((x - -10) - (canvas.width/2))/(canvas.width/2));
        y = (12 * (canvas.height/2 - (y - -10))/(canvas.height/2));
        // Push the mousePoints into corresponding arrays
        xMousePoints.push(x);
        yMousePoints.push(y);
        // If four points have been selected
        if(xMousePoints.length == 4) {
            // Push the control points
            xControlPoints.push([xMousePoints[0],xMousePoints[1],xMousePoints[2],xMousePoints[3]]);
            yControlPoints.push([yMousePoints[0],yMousePoints[1],yMousePoints[2],yMousePoints[3]]);
            // Call this to push them to the vertex buffer
            generateBCurve();
            // Update the vertex buffer
            buffer();
            // Reset the mouse point arrays
            xMousePoints.length = yMousePoints.length = 0;
        }
    });

    render();
}

function buffer() {
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
}

var render = function() 
{
    // Clear the buffer and depth buffer bits
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //eye = vec3(Math.sin(eye_y)*far,10.0,Math.cos(eye_y)*far);

    // Set the modelViewMatrix based on the eye, at, and up variables
    modelViewMatrix = lookAt(eye, at, up);
    // Set the field of view
    fovy = 50;
    // Set up the matrix properties for the projection and modelView matrices
    projectionMatrix = perspective(fovy,aspect,near,far);
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));
    gl.uniformMatrix4fv(projectionMatrixLoc, false, flatten(projectionMatrix));
    // If there are vertices in the buffer, draw them
    if(numVertices != 0) { gl.drawArrays(gl.LINE_STRIP, 0, numVertices); }
    // Request the animation frame
    requestAnimFrame(render);
}