// Specify what is going to be outputted (the vertices)
var textToSave = vertices;
var textRead = "";

// This function is the same as the one provided in class
function saveToFile() {
    var textBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textBlob);
    var saveFileName = document.getElementById("saveFileName").value;
    var downloadLink       = document.createElement("a");
    downloadLink.download  = saveFileName;
    downloadLink.innerHTML = "Download File";
    downloadLink.href      = textToSaveAsURL;
    downloadLink.onclick   = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

// This function is the same as the one provided in class
function destroyClickedElement(event) { document.body.removeChild(event.target); }

// This function is also the same as the one provided in class, but with some modifications
function loadFromFile() {

    var fileToLoad = document.getElementById("fileToLoad").files[0];
 
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) {
    
        var textFromFileLoaded = fileLoadedEvent.target.result;
        
        textRead = textFromFileLoaded;

        // Initialize the count for the subarrays
        var count = 0;
        // Split up the entire vertices by the comma delimiter
        var verticesArray = textRead.split(",");
        // Push the subarray, as well as the vertex
        vertices.push([]);
        vertices[count].push(parseFloat(verticesArray[0]));
        vertices[count].push(parseFloat(verticesArray[1]));
        vertices[count].push(parseFloat(verticesArray[2]));
        vertices[count].push(parseFloat(verticesArray[3]));
        // Increment numVertices
        numVertices += 1;
        // Iterate through the entive vertices array
        for(var i = 4; i < verticesArray.length; i++) {
            // If the last parameter of the point has been reached
            if(i%4 == 0) {
                // Increment count and push in a new subarray
                count++;
                vertices.push([]);
                // Push the point into this new subarray and increment numVertices
                vertices[count].push(parseFloat(verticesArray[i]));
                numVertices++;
                // Update the buffer
                buffer();
            }
            else {
                // Push the point into the subarray and update the buffer
                vertices[count].push(parseFloat(verticesArray[i]));
                buffer(); 
            }
        }
    };
    fileReader.readAsText(fileToLoad, "UTF-8");
}