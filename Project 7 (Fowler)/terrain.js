class Terrain {
    constructor(gridAmount, texture,xtrans,ytrans,ztrans) {
        // Initialize variables
        var indexPoints = [];
        var hMax = 1.06;
        var hMin = 1.0;

        // This for loop essentially goes through and generates y values depending on the current index.
        // It will then push the xyz point into terrainPoints
        for(var i = 0; i < gridAmount; i++) {
            terrainPoints.push([]);
            for(var j = 0; j < gridAmount; j++) {
                if(j < 5) { var y = i/20 * Math.sin(j) * Math.sin(Math.random()*(hMax - hMin)/hMin); }
                else if(j < 20) { y = i/7 * Math.sin(j) * Math.sin(Math.random()*(hMax - hMin)/hMin); } 
                else if(j < 30) { y = i/3 * Math.sin(Math.random()*(hMax - hMin)/hMin); }    
                else if(j < 40) { y = i/2 * Math.sin(Math.random()*(hMax - hMin)/hMin); }
                else { y = i * Math.sin(Math.random()*(hMax - hMin)/hMin); } 
                terrainPoints[i].push(vec4(i+xtrans,y+ytrans,j+ztrans,1.0));
            }
        }

        // This loop takes the vertices that make up where the two triangles will go and pushes them to
        // a temporary array that will be used in determining the normal, color, point, and texture vertices.
        for(var i = 0; i < gridAmount-1; i++) {
            for(var j = 0; j < gridAmount-1; j++) {
                indexPoints.push(terrainPoints[i+1][j+1]); // Vertex 0
                indexPoints.push(terrainPoints[i+1][j]);   // Vertex 1
                indexPoints.push(terrainPoints[i][j]);     // Vertex 2
                indexPoints.push(terrainPoints[i][j+1]);   // Vertex 3
                pushPoints();
            }
        }

        function pushPoints() {
            // Used to define the corners of the texture image
            var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points[texture].push( indexPoints[indices[i]] );
                normalsArray[texture].push(normal);

                // Color the two triangles white
                colors[texture].push(vec4(1.0, 1.0, 1.0, 1.0 ));

                // Alter which texture coordinate to push into depending on i
                switch(i) {
                    case 0: texCoordsArray[texture].push(texCoord[0]); break;
                    case 1: texCoordsArray[texture].push(texCoord[1]); break;
                    case 2: texCoordsArray[texture].push(texCoord[2]); break;
                    case 3: texCoordsArray[texture].push(texCoord[0]); break;
                    case 4: texCoordsArray[texture].push(texCoord[2]); break;
                    default: texCoordsArray[texture].push(texCoord[3]); break;
                }
                pointsCount[texture] += 1; // Increment pointsCount for that texture
            }
            indexPoints = [];              // Reset indexPoints to handle the next four vertices
        }
    }
}