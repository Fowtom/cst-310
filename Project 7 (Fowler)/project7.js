"use strict";

// Initialize canvas and gl variables
var canvas;
var gl;

// Used to store the data
var points = [[],[],[],[]];
var normalsArray = [[],[],[],[]];
var colors = [[],[],[],[]];
var texCoordsArray = [[],[],[],[]];
var pointsCount = [0,0,0,0];
var terrainPoints = [];

// Used for LSystem
var treeSequence = [];
var topPoint;

// Where the seeds are stored
var treeVertices = [];

var treeFlag = 0;
var lsys1flag = 0;
var lsys2flag = 0;
var lsys3flag = 0;
var lsys4flag = 0;

// Used for rotation and scaling
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];
var thetaLoc;
var scale = 0.5;
var scaleLoc;

// Used for camera positioning
var near = 1;
var far = 100;
var fovy = 45.0;
var aspect;
var flag = false;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;
var eyePt = vec3(0, 0, 5);
const atPt = vec3(0.0, 0.0, -100);
const upVec = vec3(0.0, 1.0, 0.0);

// These variables handle the light aspects
var lightPosition = vec4(0.0, 10.0, 200.0, 0.0 );
var materialShininess = 100.0;
var lightAmbient = vec4(0.1, 0.1, 0.1, 1.0 );
var lightDiffuse = vec4( .5, .5, .5, 1.0 );
var lightSpecular = vec4( .5, .5, .5, 1.0 );
var materialAmbient = vec4( .1, .1, .1, 1.0 );
var materialDiffuse = vec4( 1, 1, 1, 1.0);
var materialSpecular = vec4( .1, .1, .1, 1.0 );

var forest;

// Used for loading the shaders
var program;

// Used for loading the textures
var imageTexture = [
    'https://farm5.staticflickr.com/4614/40539845282_69b7b09552_z.jpg', // Wood  (0)
    'https://farm5.staticflickr.com/4801/26887289958_ca99bfbf91_z.jpg', // Grass (1)
    'https://farm1.staticflickr.com/789/39354589850_b74c572cea_z.jpg',  // Leaves (2)
    'https://farm5.staticflickr.com/4780/40709305152_50686caf83_z.jpg'  // Transparent (3)
];
var imageLoadedCount = 0;
var fs_textureLoc;

function LSystem(value, translation, value2, translation2, n) {
    var newTranslation = "";                                   // Initialize a string to represent the output
    for(var i = 0; i < n-1; i++) {                             // Loop through n - 1 many times
        for(var j = 0; j < translation.length; j++) {          // Loop through the length of the translation
            var char = translation.charAt(j);                  // Set variable char to the character at the position
            if(char == value) {                                // If the value in the parameter is the same as the character.
                newTranslation += translation;                 // Append the output by the translation
            }
            else if(char == value2) {                          // If the second value in the parameter is the same as the character.
                newTranslation += translation2;                // Append the output by the second translation
            }
            else { newTranslation += char }                    // Otherwise, just append the character
        }
    }
    treeSequence.push(newTranslation);                         // Push the LSystem sequence to an array for later use
}

function buildEverything() {
    switch(treeFlag) {
        case 0:
            // for(var i = 0; i < 5; i++) { forest.generateTree(i,1,0.5,25.7,25.7,25.7); }
            // for(var i = 5; i < 10; i++) { forest.generateTree(i,1,0.5,22.5,22.5,22.5); }
            // for(var i = 10; i < 15; i++) { forest.generateTree(i,1,0.5,28,28,28); }
            // for(var i = 15; i < 20; i++) { forest.generateTree(i,1,0.5,22.5,22.5,22.5); }
            break;
        case 1:
            for(var i = 0; i < 5; i++) { forest.generateTree(i,1,0.5,25.7,25.7,25.7); } break;
        case 2:
            for(var i = 5; i < 10; i++) { forest.generateTree(i,1,0.5,22.5,22.5,22.5); } break;
        case 3:
            for(var i = 10; i < 15; i++) { forest.generateTree(i,1,0.5,28,28,28); } break;
        case 4:
            for(var i = 15; i < 20; i++) { forest.generateTree(i,1,0.5,22.5,22.5,22.5); } break;
    }
    treeFlag = 0;
    lsys1flag = 0;
    lsys2flag = 0;
    lsys3flag = 0;
    lsys4flag = 0;
}

window.onload = function init() {
    
    // Get the canvas and shaders from the HTML
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    program  = initShaders( gl, "vertex-shader", "fragment-shader" );

    // Specify canvas properties
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 0.0 );
    gl.enable(gl.DEPTH_TEST);     

    //  Compile, Link shaders into program
    gl.useProgram( program );

    // Buttons
    document.getElementById( "xButton" ).onclick = function () { theta[xAxis] = 0; axis = xAxis; flag = true; };
    document.getElementById( "yButton" ).onclick = function () { theta[yAxis] = 0; axis = yAxis; flag = true; };
    document.getElementById( "zButton" ).onclick = function () { theta[zAxis] = 0; axis = zAxis; flag = true; };
    document.getElementById( "scaleUpButton" ).onclick = function () { scale += 0.005; };
    document.getElementById( "scaleDownButton" ).onclick = function () { scale -= 0.005; };
    document.getElementById( "freezeButton" ).onclick = function () { flag = !flag; };
    document.getElementById( "resetButton" ).onclick = function () {
        document.getElementById("fovySlider").value = 0;
        document.getElementById("xEyeSlider").value = 0;
        document.getElementById("yEyeSlider").value = 0;
        document.getElementById("zEyeSlider").value = 5;
        document.getElementById("xCtrSlider").value = 0;
        document.getElementById("yCtrSlider").value = 0;
        document.getElementById("zCtrSlider").value = -100;
        document.getElementById("xUpSlider").value = 0;
        document.getElementById("yUpSlider").value = 1;
        document.getElementById("zUpSlider").value = 1;
        document.getElementById("xSlider").value = 0;
        document.getElementById("ySlider").value = 0;
        document.getElementById("zSlider").value = 0;
        document.getElementById("xLightSlider").value = 0;
        document.getElementById("yLightSlider").value = 10;
        document.getElementById("zLightSlider").value = 200;
        fovy = 45;
        eyePt[0] = 0; eyePt[1] = 0; eyePt[2] = 5;
        atPt[0] = 0; atPt[1] = 0; atPt[2] = -100;
        upVec[0] = 0; upVec[1] = 1; upVec[2] = 0;
        theta[xAxis] = 0; theta[yAxis] = 0; theta[zAxis] = 0;
        lightPosition[0] = 0; lightPosition[1] = 10; lightPosition[2] = 200;
        flag = false;
    };
    document.getElementById( "lsys1Button" ).onclick = function () { lsys1flag = !lsys1flag; };
    document.getElementById( "lsys2Button" ).onclick = function () { lsys2flag = !lsys2flag; };
    document.getElementById( "lsys3Button" ).onclick = function () { lsys3flag = !lsys3flag; };
    document.getElementById( "lsys4Button" ).onclick = function () { lsys4flag = !lsys4flag; };


    // L System
    // Call the LSystem function for all of the sequences
    for(var i = 0; i < 5; i++) { LSystem("F","F[+F]F[-F]F",0,0,6); }                   // lsys1.txt
    for(var i = 0; i < 5; i++) { LSystem("F","FF-[-F+F+F]+[+F-F-F]",0,0,5); }          // lsys2.txt
    for(var i = 0; i < 5; i++) { LSystem("F","F[&+F]F[-/F][-/F][&F]",0,0,4); }         // lsys3.txt
    for(var i = 0; i < 5; i++) { LSystem("F","F[-&*F][*++&F]||F[--&/F][+&F]",0,0,4); } // lsys4.txt
    
    // Display results in the HTML
    document.getElementById("lsys1").innerHTML = "Lsys1: " + treeSequence[0];
    document.getElementById("lsys2").innerHTML = "Lsys2: " + treeSequence[5];
    document.getElementById("lsys3").innerHTML = "Lsys3: " + treeSequence[10];
    document.getElementById("lsys4").innerHTML = "Lsys4: " + treeSequence[15];

    var terrain = new Terrain(100,1,-50,-10,-40);
    forest = new Forest(20,100);
    // Call the function for rendering each object
    buildEverything();

    // Call these functions to create and specify the buffers
	loadVertices(program);
    loadColors(program);
    loadTextures(program);

    // Load the images for the textures
    for(var i = 0; i < imageTexture.length; i++) { loadImage(program, imageTexture[i], i); }

    // Get the location of the angle
    thetaLoc = gl.getUniformLocation(program, "theta");
    scaleLoc = gl.getUniformLocation(program, "scale");

    // Sliders
    document.getElementById("fovySlider").oninput = function(event) { fovy = event.target.value; };
    document.getElementById("xEyeSlider").oninput = function(event) { eyePt[0] = event.target.value; };
    document.getElementById("yEyeSlider").oninput = function(event) { eyePt[1] = event.target.value; };
    document.getElementById("zEyeSlider").oninput = function(event) { eyePt[2] = event.target.value; };
    document.getElementById("xCtrSlider").oninput = function(event) { atPt[0] = event.target.value; };
    document.getElementById("yCtrSlider").oninput = function(event) { atPt[1] = event.target.value; };
    document.getElementById("zCtrSlider").oninput = function(event) { atPt[2] = event.target.value; };
    document.getElementById("xUpSlider").oninput = function(event) { upVec[0] = event.target.value; };
    document.getElementById("yUpSlider").oninput = function(event) { upVec[1] = event.target.value; };
    document.getElementById("zUpSlider").oninput = function(event) { upVec[2] = event.target.value; };
    document.getElementById("xLightSlider").oninput = function(event) { lightPosition[0] = event.target.value; };
    document.getElementById("yLightSlider").oninput = function(event) { lightPosition[1] = event.target.value; };
    document.getElementById("zLightSlider").oninput = function(event) { lightPosition[2] = event.target.value; };
    document.getElementById("xSlider").oninput = function(event) { theta[xAxis] = event.target.value; };
    document.getElementById("ySlider").oninput = function(event) { theta[yAxis] = event.target.value; };
    document.getElementById("zSlider").oninput = function(event) { theta[zAxis] = event.target.value; };

    setViewProjection(program); // Call this to handle camera positioning using ViewMatrix and ProjectionMatrix

    render();                   // Render the scene
}

function loadVertices(program) {
    // Load normals for the diffuse light source
    var nBuffer = gl.createBuffer();                                             // Store normal vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedNormals = [].concat.apply([],normalsArray);                     // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedNormals), gl.STATIC_DRAW ); // Vertices stored in normalsArray.
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );                // Gets variable from vertex shader
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vs_Normal );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL

    var vBuffer = gl.createBuffer();                                             // Store vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedPoints = [].concat.apply([],points);                            // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedPoints), gl.STATIC_DRAW );  // Vertices stored in points
    
    // Load the vertices for the triangles and enable the attribute vPosition
    var vPosition = gl.getAttribLocation( program, "vPosition" );                // Gets variable from vertex shader 
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vPosition );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadColors(program) {
    // Store colors in a buffer
    var cBuffer = gl.createBuffer();                                            // Store color vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedColors = [].concat.apply([],colors);                           // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedColors), gl.STATIC_DRAW ); // Vertices stored in colors

    var vColor = gl.getAttribLocation( program, "vColor" );                     // Load the colors for the triangles and enable the attribute vColor
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );                 // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vColor );                                       // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadTextures(program) {
    var tBuffer = gl.createBuffer();                                          // Store texture vertices in a buffer for the shaders to use
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedTextures = [].concat.apply([],texCoordsArray);               // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData(gl.ARRAY_BUFFER,flatten(flattenedTextures),gl.STATIC_DRAW); // Vertices stored in textures

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");               // Load the textures for the triangles and enable the attribute vTexCoord
    gl.vertexAttribPointer(vTexCoord,2,gl.FLOAT,false,0,0);                   // Specifies organization of data in the arrays
    gl.enableVertexAttribArray(vTexCoord);                                    // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadImage(program, url, txtUnit) {
    var texture = gl.createTexture();                                       // Create a texture object
    fs_textureLoc = gl.getUniformLocation(program, "fs_texture");           // Get the location of the fragment shader texture parameter
    var image = new Image();                                                // Create an image object
    image.onload = function() { loadTexture(image, texture, txtUnit); };    // Set up an event handler that goes off when an image is finished loading
    image.crossOrigin = "anonymous";                                        // Make it so the CORS test passes
    image.src = url;                                                        // Load image from URL
}

function loadTexture(image, texture, txtUnit) {
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);                              // Flip the Y-coordinates
    gl.activeTexture( gl.TEXTURE0+txtUnit );                                // Assign the active texture to internal texture unit 0 for rendering
    gl.bindTexture(gl.TEXTURE_2D, texture);                                 // Bind the texture
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,gl.LINEAR);       // Assume the image is of a power of 2
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,image);  // Create the 2D image from the texture
    imageLoadedCount += 1;                                                  // Increment the imageLoadedCount
}

function setViewProjection( program ){
    VMatrixLoc = gl.getUniformLocation( program, "vs_ViewMatrix" );          // Gets the location of the view matrix
    PMatrixLoc = gl.getUniformLocation( program, "vs_ProjMatrix" );          // Gets the location of the projection matrix

    // Return error message if the location cannot be found
    if(!VMatrixLoc || !PMatrixLoc) { console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix'); return; }

    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                              // Set the view matrix depending on the camera coordinates
    aspect = canvas.width/canvas.height;                                     // Set the aspect to ratio of canvas width and height
    vs_ProjMatrix = perspective(fovy, aspect, near, far);                    // Set the projection matrix depending on the camera coordinates

    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix));         // Set up the uniform matrix for the view matrix
    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix));         // Set up the uniform matrix for the projection matrix
}

function loadPhongModel() {
    var ambientProduct = mult(lightAmbient, materialAmbient);                // Calculate the ambientProduct
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);                // Calculate the diffuseProduct
    var specularProduct = mult(lightSpecular, materialSpecular);             // Calculate the specularProduct

    gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"), flatten(ambientProduct) );   // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"), flatten(diffuseProduct) );   // Set up the uniform matrix for the diffuse
    gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"), flatten(specularProduct) ); // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"), flatten(lightPosition) );     // Set up the uniform matrix for the light position
    gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"), materialShininess);                // Set up the uniform matrix for the shininess
}

// This function creates the dynamic values displayed on the UI as the sliders are being used
function updateValuesDisplay() 
{
    var el = document.querySelector('#eyeText');
    el.innerHTML = '<strong>Eye ( '+ eyePt[0] + ', ' + eyePt[1] + ', ' + eyePt[2] + ' )</strong>';

    el = document.querySelector('#centerText');
    el.innerHTML = '<strong>Center ( '+ atPt[0] + ', ' + atPt[1] + ', ' + atPt[2] + ' )</strong>';

    el = document.querySelector('#upText');
    el.innerHTML = '<strong>Up ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[1] + ' )</strong>';

    el = document.querySelector('#fovyText');
    el.innerHTML = '<strong>Field of View ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[2] + ' )</strong>';

    var el = document.querySelector('#lightText');
    el.innerHTML = '<strong>Light ( '+ lightPosition[0] + ', ' + lightPosition[1] + ', ' + lightPosition[2] + ' )</strong>';

    var el = document.querySelector('#xSliderText');
    el.innerHTML = '<strong>X-Direction ( '+ theta[xAxis] + ' )</strong>';

    var el = document.querySelector('#ySliderText');
    el.innerHTML = '<strong>Y-Direction ( '+ theta[yAxis] + ' )</strong>';

    var el = document.querySelector('#zSliderText');
    el.innerHTML = '<strong>Z-Direction ( '+ theta[zAxis] + ' )</strong>';
}

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );                // Create the window on the screen
    updateValuesDisplay();                                                // Call this to update UI

    if(flag) theta[axis] += 1.0;                                          // If a rotate or freeze button has been pressed, react accordingly

    // Call generateTree 5 times for each Lsys sequence.
    if(lsys1flag) { treeFlag = 1; buildEverything();
        loadVertices(program);
        loadColors(program);
        loadTextures(program);}   // lsys1
    if(lsys2flag) { treeFlag = 2; buildEverything();
        loadVertices(program);
        loadColors(program);
        loadTextures(program);}  // lsys2
    if(lsys3flag) { treeFlag = 3; buildEverything();
        loadVertices(program);
        loadColors(program);
        loadTextures(program);}       // lsys3
    if(lsys4flag) { treeFlag = 4; buildEverything();
        loadVertices(program);
        loadColors(program);
        loadTextures(program);} // lsys4

    gl.uniform3fv(thetaLoc, theta);                                       // Account for the location of theta
    gl.uniform1f(scaleLoc, scale);                                        // Account for the location of scale

    vs_ProjMatrix = perspective(fovy, aspect, near, far);                 // Set the position of the projection matrix
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                           // Set the position of the view matrix

    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0])); // Handle rotation in the x-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0])); // Handle rotation in the y-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1])); // Handle rotation in the z-direction

    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix) );     // Set up the uniform matrix for the projection matrix
    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix) );     // Set up the uniform matrix for the view matrix
    loadPhongModel();                                                     // Call loadPhongModel()

    //buildEverything();

    if(imageLoadedCount == imageTexture.length) {                         // If all of the images have been loaded
    
        var objOffset = 0;                                                // Initialize the offset value
        for (var txtUnit = 0; txtUnit < imageTexture.length; txtUnit++) { // Loop through the number of faces
        
            gl.uniform1i(fs_textureLoc, txtUnit);                         // Reference the location of the fragment shader for the texture
            gl.drawArrays(gl.TRIANGLES, objOffset,pointsCount[txtUnit]);  // Draw out all of the textures
            objOffset += pointsCount[txtUnit];                            // Increment the offset
        }
    }
    requestAnimFrame( render );                                           // Call the render function recursively for each frame
}