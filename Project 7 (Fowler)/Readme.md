Thomas Fowler
CST-310, MWF155A
Jeff Griffith
31 March 2018

PROJECT 7: FRACTAL FOREST

In this project, I incorporated L-systems, polycylinders, and a terrain mesh to create a forest. It involved parsing through the specified L-System grammars and generating the tree based on the defined instructions for each character in the grammar.

DIRECTIONS TO RENDER THE FOREST
1) Ensure that project7.html and project7.js are both in the same directory, and the folder that contains both of these is in the same directory as the "Common" folder with all of the extra javascript files.
2) Open "project7.html"
3) If executed correctly, the forest interface should appear within your default web browser.
4) Press the "Lsys1" - "Lsys4" buttons to generate the trees of the forest.

