class Sphere {
    constructor(iterations,radius,texture,xtrans,ytrans,ztrans) {
        // Initialize variables
        var thet, phi = 0;
        var thePoints = [];
        var indexPoints = [];
        var index = 0;

        // Create a nested for loop that goes through the columns and rows of the sphere (latitudinal)
        for(var i=0; i<iterations + 1; i++) {
            thePoints.push([]);
            for(var j=0; j<iterations; j++) {
                // Determine theta, phi, and the xyz coordinates
                thet = i * 2 * Math.PI/iterations;
                phi = j * Math.PI/iterations;

                var x = radius * Math.cos(thet) * Math.sin(phi) + xtrans;
                var y = radius * Math.sin(thet) * Math.sin(phi) + ytrans;
                var z = radius * Math.cos(phi) + ztrans;
                thePoints[i].push(vec4(x,y,z,1));
            }
            index += 1;
        }

        // Create a nested for loop that goes through the columns and rows of the sphere (longitudinal)
        for(var j=0; j<iterations; j++) {
            thePoints.push([]);
            for(var i=0; i<iterations + 1; i++) {
                // Determine theta, phi, and the xyz coordinates
                thet = i * 2 * Math.PI/iterations;
                phi = j * Math.PI/iterations;

                var x1 = radius * Math.cos(thet) * Math.sin(phi) + xtrans;
                var y1 = radius * Math.sin(thet) * Math.sin(phi) + ytrans;
                var z1 = radius * Math.cos(phi) + ztrans;
                thePoints[index].push(vec4(x1,y1,z1,1));
            }
            index += 1;
        }

        // This loop takes the vertices that make up where the two triangles will go and pushes them to
        // a temporary array that will be used in determining the normal, color, point, and texture vertices.
        for(var i = 0; i < iterations; i++) {
            for(var j = 0; j < iterations-1; j++) {
                indexPoints.push(thePoints[i+1][j+1]); // Vertex 0
                indexPoints.push(thePoints[i+1][j]);   // Vertex 1
                indexPoints.push(thePoints[i][j]);     // Vertex 2
                indexPoints.push(thePoints[i][j+1]);   // Vertex 3
                pushPoints();
            }
        }

        function pushPoints() {
            // Used to define the corners of the texture image
            var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points[texture].push( indexPoints[indices[i]] );
                normalsArray[texture].push(normal);

                // Color the two triangles white
                colors[texture].push(vec4(1.0, 1.0, 1.0, 1.0 ));

                // Alter which texture coordinate to push into depending on i
                switch(i) {
                    case 0: texCoordsArray[texture].push(texCoord[0]); break;
                    case 1: texCoordsArray[texture].push(texCoord[1]); break;
                    case 2: texCoordsArray[texture].push(texCoord[2]); break;
                    case 3: texCoordsArray[texture].push(texCoord[0]); break;
                    case 4: texCoordsArray[texture].push(texCoord[2]); break;
                    default: texCoordsArray[texture].push(texCoord[3]); break;
                }
                pointsCount[texture] += 1; // Increment pointsCount for that texture
            }
            indexPoints = [];              // Reset indexPoints to handle the next four vertices
        }
    }
}