class Forest {
    constructor(numTrees,gridAmount) {

        generateSeeds();

        function generateSeeds() {
            for(var i = 0; i < numTrees; i++) {
                // Push an array for that tree
                treeVertices.push([]);
                // Randomly generate a point on the terrain
                var seed1 = Math.floor(Math.random()*gridAmount);
                var seed2 = Math.floor(Math.random()*gridAmount);
                var joint = terrainPoints[seed1][seed2];
                // Push the starting point for each tree
                treeVertices[i].push(joint); 
            }
        }
    }

    generateTree(seedIndex,len,radius,xAngle,yAngle,zAngle) {
        // Generates the starting position
        var pointStack = [];
        var angleXStack = [];
        var angleYStack = [];
        var angleZStack = [];
        var radiusStack = [];

        // Initialize the angles and radius
        var currentXAngle = 90;
        var currentYAngle = Math.floor(Math.random()*360);
        var currentZAngle = 0;
        var currentRadius = radius;

        // Keeps track of which point of the current tree we are on
        var treePointIndex = 0;

        // Initialize the current point
        var currentPoint = treeVertices[seedIndex][treePointIndex];

        // Iterate through all of the characters of a given grammar
        for(var i = 0; i < treeSequence[seedIndex].length; i++) {
            
            // Keep track of current character in grammar
            var currentChar = treeSequence[seedIndex][i];
            
            switch(currentChar) {
                case "F":
                    // Generate a new cylinder
                    var cyl = new Cylinder(30,len,currentRadius,0,currentXAngle,currentYAngle,currentZAngle,currentPoint[0],currentPoint[1],currentPoint[2]);

                    // Update the current point to the top of the cylinder
                    currentPoint = topPoint;

                    // Create a sphere at the end to create the polycylinder
                    var sph = new Sphere(10,currentRadius,2,currentPoint[0],currentPoint[1],currentPoint[2]);

                    // Push the current point to treeVertices[seedIndex]
                    treeVertices[seedIndex].push(currentPoint);

                    // Increment treePointIndex and radius
                    treePointIndex += 1;
                    currentRadius *= 0.92;
                    break;
                        
                case "+":
                    currentXAngle += xAngle; break;
                case "&":
                    currentYAngle += yAngle; break;
                case "-":
                    currentXAngle -= xAngle; break;
                case "/":
                    currentZAngle -= zAngle; break;
                case "*":
                    currentZAngle += zAngle; break;
                case "|":
                    currentYAngle += 180; break;
                case "[":
                    // Push all of the details to each stack
                    pointStack.push(currentPoint);
                    radiusStack.push(currentRadius);
                    angleXStack.push(currentXAngle);
                    angleYStack.push(currentYAngle);
                    angleZStack.push(currentZAngle);
                    break;
                case "]":
                    // Pop all of the details from each stack
                    currentPoint = pointStack.pop();
                    currentRadius = radiusStack.pop();
                    currentXAngle = angleXStack.pop();
                    currentYAngle = angleYStack.pop();
                    currentZAngle = angleZStack.pop();
                    break;
            }
        }
    }
}