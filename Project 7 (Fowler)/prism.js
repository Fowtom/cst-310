
class Plane {
    // Whenever a new plane object is built, each of these parameters specify its size, color, and location.
    constructor(length,height,depth,a,b,c,d,texture,xtrans,ytrans,ztrans) {

        // Change vertex values by the parameter specifications
        var vertices = [
            vec4( -length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 0
            vec4( -length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 1
            vec4(  length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 2
            vec4(  length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 3
            vec4( -length+xtrans, -height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 4
            vec4( -length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 5
            vec4(  length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 6
            vec4(  length+xtrans, -height+ytrans, -depth+ztrans, 1.0 )   // Vertex 7
        ];

        // Used to define the corners of the texture image
        var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

        // Get the normal
        var v1 = subtract(vertices[b], vertices[a]);
        var v2 = subtract(vertices[c], vertices[b]);
        var normal = cross(v1, v2);
        normal = vec3(normal);

        // Create the plane with two triangles and push each vertex to the points array.
        var indices = [ a, b, c, a, c, d ];
        for ( var i = 0; i < indices.length; ++i ) {

            // Add to array each triangle
            points[texture].push( vertices[indices[i]] );
            normalsArray[texture].push(normal);

            // Color the two triangles white
            colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

            switch(i) {
                case 0: texCoordsArray[texture].push(texCoord[0]); break;
                case 1: texCoordsArray[texture].push(texCoord[1]); break;
                case 2: texCoordsArray[texture].push(texCoord[2]); break;
                case 3: texCoordsArray[texture].push(texCoord[0]); break;
                case 4: texCoordsArray[texture].push(texCoord[2]); break;
                default: texCoordsArray[texture].push(texCoord[3]); break;
            }

            pointsCount[texture] += 1;
        }
    }
}

class Prism {
    // This class essentially creates six plane objects, one for each face of the prism.
    constructor(length,height,depth,texture,xtrans,ytrans,ztrans) {
        var base = new Plane(length,height,depth,3,0,4,7,texture,xtrans,ytrans,ztrans);
        var top = new Plane(length,height,depth,6,5,1,2,texture,xtrans,ytrans,ztrans);
        var front = new Plane(length,height,depth,4,5,6,7,texture,xtrans,ytrans,ztrans);
        var left = new Plane(length,height,depth,5,4,0,1,texture,xtrans,ytrans,ztrans);
        var back = new Plane(length,height,depth,1,2,3,0,texture,xtrans,ytrans,ztrans);
        var right = new Plane(length,height,depth,2,3,7,6,texture,xtrans,ytrans,ztrans);
    }
}