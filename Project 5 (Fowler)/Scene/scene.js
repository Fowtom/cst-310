"use strict";

// Initialize canvas and gl variables
var canvas;
var gl;

// Used to store the data
var points = [[],[],[],[],[],[],[],[],[],[],[]];
var normalsArray = [[],[],[],[],[],[],[],[],[],[],[]];
var colors = [[],[],[],[],[],[],[],[],[],[],[]];
var texCoordsArray = [[],[],[],[],[],[],[],[],[],[],[]];
var pointsCount = [0,0,0,0,0,0,0,0,0,0,0];

// Used for rotation and scaling
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];
var thetaLoc;
var scale = 0.0115;
var scaleLoc;

// Used for camera positioning
var near = 1;
var far = 100;
var fovy = 45.0;
var aspect;
var flag = false;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;
var eyePt = vec3(0, 0, 5);
const atPt = vec3(0.0, 0.0, -100);
const upVec = vec3(0.0, 1.0, 0.0);

// These variables handle the light aspects
var lightPosition = vec4(0.0, 10.0, 200.0, 0.0 );
var materialShininess = 100.0;
var lightAmbient = vec4(0.1, 0.1, 0.1, 1.0 );
var lightDiffuse = vec4( .5, .5, .5, 1.0 );
var lightSpecular = vec4( .5, .5, .5, 1.0 );
var materialAmbient = vec4( .1, .1, .1, 1.0 );
var materialDiffuse = vec4( 1, 1, 1, 1.0);
var materialSpecular = vec4( .1, .1, .1, 1.0 );

// Used for loading the shaders
var program;

// Used for loading the textures
var imageTexture = [
    'https://farm5.staticflickr.com/4793/39687518245_74b90426b1_z.jpg', // Wall
    'https://farm5.staticflickr.com/4765/40539845452_a43ac5dff9_z.jpg', // Floor
    'https://farm5.staticflickr.com/4740/39687518525_037e562be3_z.jpg', // Bed
    'https://farm5.staticflickr.com/4614/40539845282_69b7b09552_z.jpg', // Wood
    'https://farm5.staticflickr.com/4724/39687518405_26742af964_z.jpg', // Blinds
    'https://farm5.staticflickr.com/4631/39705135045_a070e1cdb7_z.jpg', // Wall Strip
    'https://farm5.staticflickr.com/4607/39705135105_b40ec58f53_z.jpg', // Outlet
    'https://farm5.staticflickr.com/4714/39705135155_7d2effc13a_z.jpg', // Metal
    'https://farm5.staticflickr.com/4663/39705135285_a5d614c516_z.jpg', // Fridge
    'https://farm5.staticflickr.com/4704/40601980941_d5f6d7903b_z.jpg', // TLOU Poster
    'https://farm5.staticflickr.com/4697/39890927234_92b74aea08_z.jpg'  // Persona Poster
];
var imageLoadedCount = 0;
var fs_textureLoc;

// All of these variables are used in the following functions that are called.
var wallFloorScale = 100;
var cornerScale = 3;
var cornerHeight = 50;
var beamLength = 28;
var beamHeight = 4;
var beamDepth = 1.5;
var metalFramePieceLength = 30;
var metalFramePieceHeight = 3;
var metalFramePieceDepth = 66;
var supportLatchScale = 1.1;
var supportLatchHeight = 30;
var connectingLatchLength = 5;
var connectingLatchHeight = 30;
var connectingLatchDepth = 5;
var fridgeScale = 18;
var fridgeHeight = 30;
var fridgeDoorWidth = 3;
var wallStripHeight = 4;
var wallSegmentSideLength = 25;
var wallTopLength = 40;
var wallTopHeight = 25;
var wallBottomHeight = 35;
var windowPaneBaseLength = 40;
var windowPaneSideLength = 60;
var windowPaneHeight = 40;
var windowPaneDepth = 9;
var blindLength = 3;
var blindHeight = 38.5;
var blindDepth = 1;
var extrusionScale = 25;
var outletLength = 1;
var outletHeight = 5;
var outletDepth = 3;
var bedLength = 24;
var bedHeight = 60;
var bedDepth = 56;
var bedSideLength = 4;
var bedSideHeight = 4;
var bedSideDepth = 56;
var gearCaseScale = 4;

class Plane {
    // Whenever a new plane object is built, each of these parameters specify its size, color, and location.
    constructor(length,height,depth,a,b,c,d,texture,xtrans,ytrans,ztrans) {

        // Change vertex values by the parameter specifications
        var vertices = [
            vec4( -length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 0
            vec4( -length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 1
            vec4(  length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 2
            vec4(  length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 3
            vec4( -length+xtrans, -height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 4
            vec4( -length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 5
            vec4(  length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 6
            vec4(  length+xtrans, -height+ytrans, -depth+ztrans, 1.0 )   // Vertex 7
        ];

        // Used to define the corners of the texture image
        var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

        // Get the normal
        var v1 = subtract(vertices[b], vertices[a]);
        var v2 = subtract(vertices[c], vertices[b]);
        var normal = cross(v1, v2);
        normal = vec3(normal);

        // Create the plane with two triangles and push each vertex to the points array.
        var indices = [ a, b, c, a, c, d ];
        for ( var i = 0; i < indices.length; ++i ) {

            // Add to array each triangle
            points[texture].push( vertices[indices[i]] );
            normalsArray[texture].push(normal);

            // Color the two triangles white
            colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

            switch(i) {
                case 0: texCoordsArray[texture].push(texCoord[0]); break;
                case 1: texCoordsArray[texture].push(texCoord[1]); break;
                case 2: texCoordsArray[texture].push(texCoord[2]); break;
                case 3: texCoordsArray[texture].push(texCoord[0]); break;
                case 4: texCoordsArray[texture].push(texCoord[2]); break;
                default: texCoordsArray[texture].push(texCoord[3]); break;
            }

            pointsCount[texture] += 1;
        }
    }
}

class Prism {
    // This class essentially creates six plane objects, one for each face of the prism.
    constructor(length,height,depth,texture,xtrans,ytrans,ztrans){
        var base = new Plane(length,height,depth,3,0,4,7,texture,xtrans,ytrans,ztrans);
        var top = new Plane(length,height,depth,6,5,1,2,texture,xtrans,ytrans,ztrans);
        var front = new Plane(length,height,depth,4,5,6,7,texture,xtrans,ytrans,ztrans);
        var left = new Plane(length,height,depth,5,4,0,1,texture,xtrans,ytrans,ztrans);
        var back = new Plane(length,height,depth,1,2,3,0,texture,xtrans,ytrans,ztrans);
        var right = new Plane(length,height,depth,2,3,7,6,texture,xtrans,ytrans,ztrans);
    }
}

function buildEverything() {
    // Call each of the functions
    buildWallsAndFloor();
    buildBedFrame();
    buildBed();
    buildFridge();
    buildWallBaseStrips();
    buildWindow();
    buildWallOutlet();
    buildExtras();
}

function buildWallsAndFloor() {
    // Build the floor, ceiling, and left wall
    var floor = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,3,0,4,7,1,0,-0.01,0);
    var ceiling = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,1,2,6,5,0,0,0,0);
    var leftWall = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,5,4,0,1,0,0,0,0);

    // These segments allow room for the window pane in the center of the right wall
    var rightWallLeftSegment = new Plane(wallSegmentSideLength,wallFloorScale,wallFloorScale,1,2,3,0,0,-75,0,0);
    var rightWallRightSegment = new Plane(wallSegmentSideLength,wallFloorScale,wallFloorScale,1,2,3,0,0,55,0,0);
    var rightWallTopSegment = new Plane(wallTopLength,wallTopHeight,wallFloorScale,1,2,3,0,0,-10,75,0);
    var rightWallBottomSegment = new Plane(wallTopLength,wallBottomHeight,wallFloorScale,1,2,3,0,0,-10,-65,0);

    // Though not included originally, there is an extrusion in the wall of my bedroom
    var extrusion = new Prism(extrusionScale,wallFloorScale,extrusionScale,0,75,0,75);
}

function buildBedFrame() {
    // Build the corner pieces of the bed frame
    var topLeftCorner = new Prism(cornerScale,cornerHeight,cornerScale,3,-96,-50,96);
    var topRightCorner = new Prism(cornerScale,cornerHeight,cornerScale,3,-36,-50,96);
    var bottomLeftCorner = new Prism(cornerScale,cornerHeight,cornerScale,3,-96,-50,-40);
    var bottomRightCorner = new Prism(cornerScale,cornerHeight,cornerScale,3,-36,-50,-40);

    // Build the connecting beams
    var connectingBeams = [];
    for(var i = 0; i < 10; i++)
    {
        if(i < 5) { connectingBeams[i] = new Prism(beamLength,beamHeight,beamDepth,3,-66,-90+20*i,94); }
        else { connectingBeams[i] = new Prism(beamLength,beamHeight,beamDepth,3,-66,-90+20*(i-5),-42); }
    }

    // Build the metal pieces for supporting the bed
    var metalFramePiece = new Prism(metalFramePieceLength,metalFramePieceHeight,metalFramePieceDepth,7,-66,-52,27);
    var supportLatch1 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,7,-96,-30,94);
    var supportLatch2 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,7,-36,-30,94);
    var supportLatch3 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,7,-96,-30,-38);
    var supportLatch4 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,7,-36,-30,-38);
}

function buildBed() {
    // Instead of building a rectangular prism to represent the bed, I decided to 
    // have edges and corner pieces to define the dimensions of the bed. The following
    // objects represent each of the bed faces.
    var bedTop = new Plane(bedLength,bedHeight,bedDepth,5,1,2,6,2,-67,-89,26);
    var bedBottom = new Plane(bedLength,bedHeight,bedDepth,1,2,6,5,2,-67,-108,26);
    var bedBack = new Plane(bedLength,bedSideHeight,bedDepth,1,2,3,0,2,-67,-38,32);
    var bedFront = new Plane(bedLength,bedSideHeight,bedDepth,4,5,6,7,2,-67,-38,19);
    var bedCore = new Prism(bedLength,9.5,bedDepth,2,-67,-39,26);

    var bedRightSide = new Plane(bedSideLength,bedSideHeight+1,bedSideDepth,6,2,3,7,2,-40,-40,26);
    var bedLeftSide = new Plane(bedSideLength,bedSideHeight+1,bedSideDepth,5,1,0,4,2,-95,-40,26);
    var bedTopRightSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,1,5,7,3,2,-40,-32,26);
    var bedTopLeftSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,6,2,0,4,2,-95,-32,26);
    var bedBottomRightSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,6,2,0,4,2,-40,-48,26);
    var bedBottomLeftSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,1,5,7,3,2,-95,-48,26);
    
    var bedTopFrontSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,4,1,2,7,2,-67,-32,-34);
    var bedTopBackSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,5,0,3,6,2,-67,-32,87);
    var bedBottomFrontSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,5,0,3,6,2,-67,-45,-34);
    var bedBottomBackSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,1,2,7,4,2,-67,-45,87);

    var cornerPieceBR = new Plane(bedSideLength,bedSideLength,bedSideLength,4,5,2,3,2,-40,-39,-33);
    var cornerPieceBL = new Plane(bedSideLength,bedSideLength,bedSideLength,1,0,7,6,2,-95,-39,-33);
    var cornerPieceTR = new Plane(bedSideLength,bedSideLength,bedSideLength,1,0,7,6,2,-40,-39,87);
    var cornerPieceTL = new Plane(bedSideLength,bedSideLength,bedSideLength,4,5,2,3,2,-95,-39,87);
}

function buildFridge() {
    // The fridge is simply two rectangular prisms adjacent to each other.
    var miniFridge = new Prism(fridgeScale,fridgeHeight,fridgeScale,7,-10,-70,78);
    var miniFridgeDoor = new Prism(fridgeScale,fridgeHeight,fridgeDoorWidth,8,-10,-70,56);
}

function buildWallBaseStrips() {
    // The base strips run along the edge of the walls, making contact with the floor
    var leftWallBaseStrip = new Plane(wallFloorScale,wallStripHeight,wallFloorScale,5,4,0,1,5,1,-96,0);
    var rightWallBaseStrip = new Plane(wallFloorScale,wallStripHeight,wallFloorScale,1,2,3,0,5,0,-96,-1);
    var leftExtrusionBaseStrip = new Plane(extrusionScale,wallStripHeight,extrusionScale,5,4,0,1,5,74,-96,74);
    var frontExtrusionBaseStrip = new Plane(extrusionScale+1,wallStripHeight,extrusionScale,1,2,3,0,5,74,-96,24);
}

function buildWindow() {
    // The window pane is from the panel that was cut out of the right wall earlier
    var windowPaneBase = new Plane(windowPaneBaseLength,windowPaneHeight,windowPaneDepth,3,0,4,7,0,-10,10,109);
    var windowPaneLeft = new Plane(windowPaneSideLength,windowPaneHeight,windowPaneDepth,5,4,0,1,0,10,10,109);
    var windowPaneTop = new Plane(windowPaneBaseLength,windowPaneHeight,windowPaneDepth,1,2,6,5,0,-10,10,109);
    var windowPaneRight = new Plane(windowPaneSideLength,windowPaneHeight,windowPaneDepth,2,3,7,6,0,-30,10,109);

    // Build all of the blinds
    var blindSupport = new Prism(39,4,5,4,-10,45,105);
    var blinds = [];
    for(var i = 0; i < 11; i++) { blinds[i] = new Prism(blindLength,blindHeight,blindDepth,4,-45+7*i,10.5,105); }
}

function buildWallOutlet() {
    // The wall outlet is a rectangular prism with two squares to represent the holes, as well as an outlet cover texture
    var outletBoard = new Prism(outletLength,outletHeight,outletDepth,0,-98,-65,-65);
    var holes1 = new Plane(1,1,1,5,4,0,1,4,-95.9,-63,-65);
    var holes2 = new Plane(1,1,1,5,4,0,1,4,-95.9,-67,-65);
    var outletCover = new Plane(outletLength,outletHeight,outletDepth,5,4,0,1,6,-95.91,-65,-65);
}

function buildExtras() {
    // All of these objects are extra to make the room look more like mine
    var gearCase = new Prism(gearCaseScale,gearCaseScale,gearCaseScale,3,0,-37,85);
    var posterTLOU = new Plane(extrusionScale-4,wallFloorScale-60,extrusionScale-4,0,1,2,3,9,75,0,28);
    var posterPersona = new Plane(wallFloorScale-60,wallFloorScale-70,wallFloorScale-70,7,6,2,3,10,-139,20,28);
}

window.onload = function init() {
    
    // Get the canvas and shaders from the HTML
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    program  = initShaders( gl, "vertex-shader", "fragment-shader" );

    // Call the function for rendering each object
    buildEverything();

    // Specify canvas properties
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.725, 1.0, 0.78, 0.0 );
    gl.enable(gl.DEPTH_TEST);     

    //  Compile, Link shaders into program
    gl.useProgram( program );

    // Call these functions to create and specify the buffers
	loadVertices(program);
    loadColors(program);
    loadTextures(program);

    // Load the images for the textures
    for(var i = 0; i < imageTexture.length; i++) { loadImage(program, imageTexture[i], i); }

    // Get the location of the angle
    thetaLoc = gl.getUniformLocation(program, "theta");
    scaleLoc = gl.getUniformLocation(program, "scale");

    // Buttons
    document.getElementById( "xButton" ).onclick = function () { theta[xAxis] = 0; axis = xAxis; flag = true; };
    document.getElementById( "yButton" ).onclick = function () { theta[yAxis] = 0; axis = yAxis; flag = true; };
    document.getElementById( "zButton" ).onclick = function () { theta[zAxis] = 0; axis = zAxis; flag = true; };
    document.getElementById( "scaleUpButton" ).onclick = function () { scale += 0.0005; };
    document.getElementById( "scaleDownButton" ).onclick = function () { scale -= 0.0005; };
    document.getElementById( "freezeButton" ).onclick = function () { flag = !flag; };
    document.getElementById( "resetButton" ).onclick = function () {
        document.getElementById("fovySlider").value = 0;
        document.getElementById("xEyeSlider").value = 0;
        document.getElementById("yEyeSlider").value = 0;
        document.getElementById("zEyeSlider").value = 5;
        document.getElementById("xCtrSlider").value = 0;
        document.getElementById("yCtrSlider").value = 0;
        document.getElementById("zCtrSlider").value = -100;
        document.getElementById("xUpSlider").value = 0;
        document.getElementById("yUpSlider").value = 1;
        document.getElementById("zUpSlider").value = 1;
        document.getElementById("xSlider").value = 0;
        document.getElementById("ySlider").value = 0;
        document.getElementById("zSlider").value = 0;
        document.getElementById("xLightSlider").value = 0;
        document.getElementById("yLightSlider").value = 10;
        document.getElementById("zLightSlider").value = 200;
        fovy = 45;
        eyePt[0] = 0; eyePt[1] = 0; eyePt[2] = 5;
        atPt[0] = 0; atPt[1] = 0; atPt[2] = -100;
        upVec[0] = 0; upVec[1] = 1; upVec[2] = 0;
        theta[xAxis] = 0; theta[yAxis] = 0; theta[zAxis] = 0;
        lightPosition[0] = 0; lightPosition[1] = 10; lightPosition[2] = 200;
        flag = false;
    };

    // Sliders
    document.getElementById("fovySlider").oninput = function(event) { fovy = event.target.value; };
    document.getElementById("xEyeSlider").oninput = function(event) { eyePt[0] = event.target.value; };
    document.getElementById("yEyeSlider").oninput = function(event) { eyePt[1] = event.target.value; };
    document.getElementById("zEyeSlider").oninput = function(event) { eyePt[2] = event.target.value; };
    document.getElementById("xCtrSlider").oninput = function(event) { atPt[0] = event.target.value; };
    document.getElementById("yCtrSlider").oninput = function(event) { atPt[1] = event.target.value; };
    document.getElementById("zCtrSlider").oninput = function(event) { atPt[2] = event.target.value; };
    document.getElementById("xUpSlider").oninput = function(event) { upVec[0] = event.target.value; };
    document.getElementById("yUpSlider").oninput = function(event) { upVec[1] = event.target.value; };
    document.getElementById("zUpSlider").oninput = function(event) { upVec[2] = event.target.value; };
    document.getElementById("xLightSlider").oninput = function(event) { lightPosition[0] = event.target.value; };
    document.getElementById("yLightSlider").oninput = function(event) { lightPosition[1] = event.target.value; };
    document.getElementById("zLightSlider").oninput = function(event) { lightPosition[2] = event.target.value; };
    document.getElementById("xSlider").oninput = function(event) { theta[xAxis] = event.target.value; };
    document.getElementById("ySlider").oninput = function(event) { theta[yAxis] = event.target.value; };
    document.getElementById("zSlider").oninput = function(event) { theta[zAxis] = event.target.value; };

    setViewProjection(program); // Call this to handle camera positioning using ViewMatrix and ProjectionMatrix
    render();                   // Render the scene
}

function loadVertices(program) {
    // Load normals for the diffuse light source
    var nBuffer = gl.createBuffer();                                             // Store normal vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedNormals = [].concat.apply([],normalsArray);                     // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedNormals), gl.STATIC_DRAW ); // Vertices stored in normalsArray.
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );                // Gets variable from vertex shader
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vs_Normal );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL

    var vBuffer = gl.createBuffer();                                             // Store vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedPoints = [].concat.apply([],points);                            // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedPoints), gl.STATIC_DRAW );  // Vertices stored in points
    
    // Load the vertices for the triangles and enable the attribute vPosition
    var vPosition = gl.getAttribLocation( program, "vPosition" );                // Gets variable from vertex shader 
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vPosition );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadColors(program) {
    // Store colors in a buffer
    var cBuffer = gl.createBuffer();                                            // Store color vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedColors = [].concat.apply([],colors);                           // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedColors), gl.STATIC_DRAW ); // Vertices stored in colors

    var vColor = gl.getAttribLocation( program, "vColor" );                     // Load the colors for the triangles and enable the attribute vColor
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );                 // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vColor );                                       // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadTextures(program) {
    var tBuffer = gl.createBuffer();                                          // Store texture vertices in a buffer for the shaders to use
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedTextures = [].concat.apply([],texCoordsArray);               // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData(gl.ARRAY_BUFFER,flatten(flattenedTextures),gl.STATIC_DRAW); // Vertices stored in textures

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");               // Load the textures for the triangles and enable the attribute vTexCoord
    gl.vertexAttribPointer(vTexCoord,2,gl.FLOAT,false,0,0);                   // Specifies organization of data in the arrays
    gl.enableVertexAttribArray(vTexCoord);                                    // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadImage(program, url, txtUnit) {
    var texture = gl.createTexture();                                       // Create a texture object
    fs_textureLoc = gl.getUniformLocation(program, "fs_texture");           // Get the location of the fragment shader texture parameter
    var image = new Image();                                                // Create an image object
    image.onload = function() { loadTexture(image, texture, txtUnit); };    // Set up an event handler that goes off when an image is finished loading
    image.crossOrigin = "anonymous";                                        // Make it so the CORS test passes
    image.src = url;                                                        // Load image from URL
}

function loadTexture(image, texture, txtUnit) {
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);                              // Flip the Y-coordinates
    gl.activeTexture( gl.TEXTURE0+txtUnit );                                // Assign the active texture to internal texture unit 0 for rendering
    gl.bindTexture(gl.TEXTURE_2D, texture);                                 // Bind the texture
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,gl.LINEAR);       // Assume the image is of a power of 2
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,image);  // Create the 2D image from the texture
    imageLoadedCount += 1;                                                  // Increment the imageLoadedCount
}

function setViewProjection( program ){
    VMatrixLoc = gl.getUniformLocation( program, "vs_ViewMatrix" );          // Gets the location of the view matrix
    PMatrixLoc = gl.getUniformLocation( program, "vs_ProjMatrix" );          // Gets the location of the projection matrix

    // Return error message if the location cannot be found
    if(!VMatrixLoc || !PMatrixLoc) { console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix'); return; }

    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                              // Set the view matrix depending on the camera coordinates
    aspect = canvas.width/canvas.height;                                     // Set the aspect to ratio of canvas width and height
    vs_ProjMatrix = perspective(fovy, aspect, near, far);                    // Set the projection matrix depending on the camera coordinates

    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix));         // Set up the uniform matrix for the view matrix
    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix));         // Set up the uniform matrix for the projection matrix
}

function loadPhongModel() {
    var ambientProduct = mult(lightAmbient, materialAmbient);                // Calculate the ambientProduct
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);                // Calculate the diffuseProduct
    var specularProduct = mult(lightSpecular, materialSpecular);             // Calculate the specularProduct

    gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"), flatten(ambientProduct) );   // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"), flatten(diffuseProduct) );   // Set up the uniform matrix for the diffuse
    gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"), flatten(specularProduct) ); // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"), flatten(lightPosition) );     // Set up the uniform matrix for the light position
    gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"), materialShininess);                // Set up the uniform matrix for the shininess
}

// This function creates the dynamic values displayed on the UI as the sliders are being used
function updateValuesDisplay() 
{
    var el = document.querySelector('#eyeText');
    el.innerHTML = '<strong>Eye ( '+ eyePt[0] + ', ' + eyePt[1] + ', ' + eyePt[2] + ' )</strong>';

    el = document.querySelector('#centerText');
    el.innerHTML = '<strong>Center ( '+ atPt[0] + ', ' + atPt[1] + ', ' + atPt[2] + ' )</strong>';

    el = document.querySelector('#upText');
    el.innerHTML = '<strong>Up ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[1] + ' )</strong>';

    el = document.querySelector('#fovyText');
    el.innerHTML = '<strong>Field of View ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[2] + ' )</strong>';

    var el = document.querySelector('#lightText');
    el.innerHTML = '<strong>Light ( '+ lightPosition[0] + ', ' + lightPosition[1] + ', ' + lightPosition[2] + ' )</strong>';

    var el = document.querySelector('#xSliderText');
    el.innerHTML = '<strong>X-Direction ( '+ theta[xAxis] + ' )</strong>';

    var el = document.querySelector('#ySliderText');
    el.innerHTML = '<strong>Y-Direction ( '+ theta[yAxis] + ' )</strong>';

    var el = document.querySelector('#zSliderText');
    el.innerHTML = '<strong>Z-Direction ( '+ theta[zAxis] + ' )</strong>';
}

// function createMesh(length,width,radius,texture,xtrans,ytrans,ztrans)
// {
//     var x = -width/2;
//     var y = 0;
//     for(var i = 0; i < width/2; i++)
//     {
//         for(var j = 0; j < length;j++)
//         {
//             x = Math.pow((Math.pow(radius,4)-Math.pow(y,4))/(1/width),1/4) + xtrans;
//             y = Math.pow((Math.pow(radius,4)-(1/width)*(Math.pow(x,4))),1/4) + ytrans;
//             z = i+ztrans;
//         }
//     }
// }

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );                // Create the window on the screen
    updateValuesDisplay();                                                // Call this to update UI

    if(flag) theta[axis] += 1.0;                                          // If a rotate or freeze button has been pressed, react accordingly
    gl.uniform3fv(thetaLoc, theta);                                       // Account for the location of theta
    gl.uniform1f(scaleLoc, scale);                                        // Account for the location of scale

    vs_ProjMatrix = perspective(fovy, aspect, near, far);                 // Set the position of the projection matrix
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                           // Set the position of the view matrix

    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0])); // Handle rotation in the x-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0])); // Handle rotation in the y-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1])); // Handle rotation in the z-direction

    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix) );     // Set up the uniform matrix for the projection matrix
    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix) );     // Set up the uniform matrix for the view matrix
    loadPhongModel();                                                     // Call loadPhongModel()

    if(imageLoadedCount == imageTexture.length) {                         // If all of the images have been loaded
    
        var objOffset = 0;                                                // Initialize the offset value
        for (var txtUnit = 0; txtUnit < imageTexture.length; txtUnit++) { // Loop through the number of faces
        
            gl.uniform1i(fs_textureLoc, txtUnit);                         // Reference the location of the fragment shader for the texture
            gl.drawArrays(gl.TRIANGLES, objOffset, pointsCount[txtUnit]); // Draw out all of the textures
            objOffset += pointsCount[txtUnit];                            // Increment the offset
        }
    }
    requestAnimFrame( render );                                           // Call the render function recursively for each frame
}