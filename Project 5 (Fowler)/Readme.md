Thomas Fowler
CST-310, MWF155A
Jeff Griffith
25 February 2018

PROJECT 5: RENDER YOUR SCENE WITH PRIMITIVES

In this project, I improved the scene I implemented in Project 4 by adding textures to the objects. It supports many different transformations, such as scaling, rotation, translation, zoom, freeze, light position, and reset. See the "Project 5: Render your Scene with Primitives (continued)" section of "Your Surrounding World (Fowler).docx" for further details.

DIRECTIONS TO RENDER THE SCENE
1) Ensure that the two folders, Scene and Common, are both in the same directory.
2) Within the Scene folder, open "scene.html"
3) If executed correctly, the scene interface should appear within your default web browser.

