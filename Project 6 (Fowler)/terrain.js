"use strict";

// Initialize canvas and gl variables
var canvas;
var gl;

// Used to store the data
var points = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
var normalsArray = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
var colors = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
var texCoordsArray = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
var pointsCount = [0,0,0,0,0,0,0,0,0,0,0,0];

// Used for LSystem
var treeSequence = [];

// Used for rotation and scaling
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];
var thetaLoc;
var scale = 0.5;
var scaleLoc;

// Used for camera positioning
var near = 1;
var far = 100;
var fovy = 45.0;
var aspect;
var flag = false;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;
var eyePt = vec3(0, 0, 5);
const atPt = vec3(0.0, 0.0, -100);
const upVec = vec3(0.0, 1.0, 0.0);

// These variables handle the light aspects
var lightPosition = vec4(0.0, 10.0, 200.0, 0.0 );
var materialShininess = 100.0;
var lightAmbient = vec4(0.1, 0.1, 0.1, 1.0 );
var lightDiffuse = vec4( .5, .5, .5, 1.0 );
var lightSpecular = vec4( .5, .5, .5, 1.0 );
var materialAmbient = vec4( .1, .1, .1, 1.0 );
var materialDiffuse = vec4( 1, 1, 1, 1.0);
var materialSpecular = vec4( .1, .1, .1, 1.0 );

// Used for loading the shaders
var program;

// Used for loading the textures
var imageTexture = [
    'https://farm5.staticflickr.com/4793/39687518245_74b90426b1_z.jpg', // Wall (0)
    'https://farm5.staticflickr.com/4765/40539845452_a43ac5dff9_z.jpg', // Floor (1)
    'https://farm5.staticflickr.com/4740/39687518525_037e562be3_z.jpg', // Bed   (2)
    'https://farm5.staticflickr.com/4614/40539845282_69b7b09552_z.jpg', // Wood  (3)
    'https://farm5.staticflickr.com/4724/39687518405_26742af964_z.jpg', // Blinds (4)
    'https://farm5.staticflickr.com/4631/39705135045_a070e1cdb7_z.jpg', // Wall Strip (5)
    'https://farm5.staticflickr.com/4607/39705135105_b40ec58f53_z.jpg', // Outlet (6)
    'https://farm5.staticflickr.com/4714/39705135155_7d2effc13a_z.jpg', // Metal (7)
    'https://farm5.staticflickr.com/4663/39705135285_a5d614c516_z.jpg', // Fridge (8)
    'https://farm5.staticflickr.com/4704/40601980941_d5f6d7903b_z.jpg', // TLOU Poster (9)
    'https://farm5.staticflickr.com/4697/39890927234_92b74aea08_z.jpg', // Persona Poster (10)
    'https://farm5.staticflickr.com/4801/26887289958_ca99bfbf91_z.jpg', // Forest (11)
    'https://farm5.staticflickr.com/4780/40709305152_50686caf83_z.jpg'  // Transparent (12)
];
var imageLoadedCount = 0;
var fs_textureLoc;

class Plane {
    // Whenever a new plane object is built, each of these parameters specify its size, color, and location.
    constructor(length,height,depth,a,b,c,d,texture,xtrans,ytrans,ztrans) {

        // Change vertex values by the parameter specifications
        var vertices = [
            vec4( -length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 0
            vec4( -length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 1
            vec4(  length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 2
            vec4(  length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 3
            vec4( -length+xtrans, -height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 4
            vec4( -length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 5
            vec4(  length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 6
            vec4(  length+xtrans, -height+ytrans, -depth+ztrans, 1.0 )   // Vertex 7
        ];

        // Used to define the corners of the texture image
        var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

        // Get the normal
        var v1 = subtract(vertices[b], vertices[a]);
        var v2 = subtract(vertices[c], vertices[b]);
        var normal = cross(v1, v2);
        normal = vec3(normal);

        // Create the plane with two triangles and push each vertex to the points array.
        var indices = [ a, b, c, a, c, d ];
        for ( var i = 0; i < indices.length; ++i ) {

            // Add to array each triangle
            points[texture].push( vertices[indices[i]] );
            normalsArray[texture].push(normal);

            // Color the two triangles white
            colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

            switch(i) {
                case 0: texCoordsArray[texture].push(texCoord[0]); break;
                case 1: texCoordsArray[texture].push(texCoord[1]); break;
                case 2: texCoordsArray[texture].push(texCoord[2]); break;
                case 3: texCoordsArray[texture].push(texCoord[0]); break;
                case 4: texCoordsArray[texture].push(texCoord[2]); break;
                default: texCoordsArray[texture].push(texCoord[3]); break;
            }

            pointsCount[texture] += 1;
        }
    }
}

class Prism {
    // This class essentially creates six plane objects, one for each face of the prism.
    constructor(length,height,depth,texture,xtrans,ytrans,ztrans) {
        var base = new Plane(length,height,depth,3,0,4,7,texture,xtrans,ytrans,ztrans);
        var top = new Plane(length,height,depth,6,5,1,2,texture,xtrans,ytrans,ztrans);
        var front = new Plane(length,height,depth,4,5,6,7,texture,xtrans,ytrans,ztrans);
        var left = new Plane(length,height,depth,5,4,0,1,texture,xtrans,ytrans,ztrans);
        var back = new Plane(length,height,depth,1,2,3,0,texture,xtrans,ytrans,ztrans);
        var right = new Plane(length,height,depth,2,3,7,6,texture,xtrans,ytrans,ztrans);
    }
}

class Cylinder {
    constructor(height,radius,texture,xtrans,ytrans,ztrans,xAngle,yAngle,zAngle) {
        // Loop through 360 times for each degree of the circle top and base
        for(var i = 0; i < 360; i++) {
            // Top circle
            var x1 = radius * Math.cos(i) + xtrans;
            var y1 = radius * Math.sin(i) + ytrans;
            var z1 = height + ztrans
            // Base circle
            var x2 = radius * Math.cos(i) + xtrans;
            var y2 = radius * Math.sin(i) + ytrans;
            var z2 = ztrans;

            switch(i) {                                 // Set up conditional for the value of i
                case 0:
                    var a = vec4(x1,y1,z1,1.0);         
                    var b = vec4(x2,y2,z2,1.0);         // Initially just increment a and b
                    a = mult(mat4(a), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    b = mult(mat4(b), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    a = mult(mat4(a), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    b = mult(mat4(b), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    a = mult(mat4(a), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
                    b = mult(mat4(b), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
                    break;
                case 1:
                    var c = vec4(x2,y2,z2,1.0);
                    var d = vec4(x1,y1,z1,1.0);         // Then increment c and d
                    c = mult(mat4(c), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    d = mult(mat4(d), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    c = mult(mat4(c), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    d = mult(mat4(d), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    c = mult(mat4(c), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
                    d = mult(mat4(d), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
                    pushPoints(); break;                // Call pushPoints to create the two triangles
                default:
                    a = d;                              // Set the next a to the previous d
                    b = c;                              // Set the next b to the previous c
                    c = vec4(x2,y2,z2,1.0);             // Set the next c
                    d = vec4(x1,y1,z1,1.0);             // Set the next d
                    c = mult(mat4(c), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    d = mult(mat4(d), rotate(xAngle, [1, 0, 0]))[0]; // Handle rotation in the x-direction
                    c = mult(mat4(c), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    d = mult(mat4(d), rotate(yAngle, [0, 1, 0]))[0]; // Handle rotation in the y-direction
                    c = mult(mat4(c), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction
                    d = mult(mat4(d), rotate(zAngle, [0, 0, 1]))[0]; // Handle rotation in the z-direction

                    pushPoints(); break;                // Call pushPoints to create the two triangles
            }
        }

        function pushPoints() {
            
            // Push all the vertices into thePoints array
            var thePoints = [];
            thePoints.push(a); // Vertex 0
            thePoints.push(b); // Vertex 1
            thePoints.push(c); // Vertex 2
            thePoints.push(d); // Vertex 3

            // Used to define the corners of the texture image
            var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

            // Get the normal
            var v1 = subtract(thePoints[1], thePoints[0]);
            var v2 = subtract(thePoints[2], thePoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points[texture].push( thePoints[indices[i]] );
                normalsArray[texture].push(normal);

                // Color the two triangles white
                colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

                // Alter which texture coordinate to push into depending on i
                switch(i) {
                    case 0: texCoordsArray[texture].push(texCoord[0]); break;
                    case 1: texCoordsArray[texture].push(texCoord[1]); break;
                    case 2: texCoordsArray[texture].push(texCoord[2]); break;
                    case 3: texCoordsArray[texture].push(texCoord[0]); break;
                    case 4: texCoordsArray[texture].push(texCoord[2]); break;
                    default: texCoordsArray[texture].push(texCoord[3]); break;
                }
                pointsCount[texture] += 1; // Increment the pointsCount for that texture
            }
        }
    }
}

class Terrain {
    constructor(gridAmount, texture,xtrans,ytrans,ztrans) {
        // Initialize variables
        var thePoints = [];
        var indexPoints = [];
        var hMax = 1.1;
        var hMin = 1.0;

        // This for loop essentially goes through and generates y values depending on the current index.
        // It will then push the xyz point into thePoints
        for(var i = 0; i < gridAmount; i++) {
            thePoints.push([]);
            for(var j = 0; j < gridAmount; j++) {
                if(j < 5) { var y = i/j * Math.sin(Math.random()*(hMax - hMin)/hMin); }
                else if(j < 20) { y = i/8 * Math.sin(Math.random()*(hMax - hMin)/hMin); } 
                else if(j < 30) { y = i/3 * Math.sin(Math.random()*(hMax - hMin)/hMin); }    
                else if(j < 40) { y = i/2 * Math.sin(j) * Math.sin(Math.random()*(hMax - hMin)/hMin); }
                else { y = i * Math.sin(j) * Math.sin(Math.random()*(hMax - hMin)/hMin); } 
                thePoints[i].push(vec4(i+xtrans,y+ytrans,j+ztrans,1.0));
            }
        }

        // This loop takes the vertices that make up where the two triangles will go and pushes them to
        // a temporary array that will be used in determining the normal, color, point, and texture vertices.
        for(var i = 0; i < gridAmount-1; i++) {
            for(var j = 0; j < gridAmount-1; j++) {
                indexPoints.push(thePoints[i+1][j+1]); // Vertex 0
                indexPoints.push(thePoints[i+1][j]);   // Vertex 1
                indexPoints.push(thePoints[i][j]);     // Vertex 2
                indexPoints.push(thePoints[i][j+1]);   // Vertex 3
                pushPoints();
            }
        }

        buildTrees();

        function buildTrees() {
            var treeVertices = [];
            var polycylinders = [];
            for(var i = 0; i < 20; i++) {
                treeVertices.push([]);
                var seed1 = Math.floor(Math.random()*gridAmount);
                var seed2 = Math.floor(Math.random()*gridAmount);
                var tree = thePoints[seed1][seed2];
                treeVertices[i].push(tree); // Push the first sphere point
                var tree1 = tree[1] += 3;
                treeVertices[i].push(tree1); // Push the next sphere point
                var tree2 = tree[1] += 6;
                treeVertices[i].push(tree2); // Push the next sphere point

                polycylinders[i] = new PolyCylinder(treeVertices[i],11,3,0.5);
            }
        }

        function pushPoints() {
            // Used to define the corners of the texture image
            var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points[texture].push( indexPoints[indices[i]] );
                normalsArray[texture].push(normal);

                // Color the two triangles white
                colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

                // Alter which texture coordinate to push into depending on i
                switch(i) {
                    case 0: texCoordsArray[texture].push(texCoord[0]); break;
                    case 1: texCoordsArray[texture].push(texCoord[1]); break;
                    case 2: texCoordsArray[texture].push(texCoord[2]); break;
                    case 3: texCoordsArray[texture].push(texCoord[0]); break;
                    case 4: texCoordsArray[texture].push(texCoord[2]); break;
                    default: texCoordsArray[texture].push(texCoord[3]); break;
                }
                pointsCount[texture] += 1; // Increment pointsCount for that texture
            }
            indexPoints = [];              // Reset indexPoints to handle the next four vertices
        }
    }
}

class Sphere {
    constructor(iterations,radius,texture,xtrans,ytrans,ztrans) {
        // Initialize variables
        var thet, phi = 0;
        var thePoints = [[],[],[],[]];
        var indexPoints = [];

        // Create a nested for loop that goes through the columns and rows of the sphere (latitudinal)
        for(var i=0; i<iterations + 1; i++) {
            for(var j=0; j<iterations; j++) {
                // Determine theta, phi, and the xyz coordinates
                thet = i * 2 * Math.PI/iterations;
                phi = j * Math.PI/iterations;

                var x1 = radius * Math.cos(thet) * Math.sin(phi) + xtrans;
                var y1 = radius * Math.sin(thet) * Math.sin(phi) + ytrans;
                var z1 = radius * Math.cos(phi) + ztrans;

                // Push the point into the a, b, c, or d subarray depending on whether or not either of the
                // iterators is even or odd
                switch(i % 2 == 0) {
                    case(true):
                        switch(j % 2 == 0) {
                            case(true):
                                thePoints[0].push(vec4(x1,y1,z1,1.0)); break;       // a
                            default:
                                thePoints[1].push(vec4(x1,y1,z1,1.0)); break;       // b
                        } break;
                    default:
                        switch(j % 2 == 0) {
                            case(true):
                                thePoints[3].push(vec4(x1,y1,z1,1.0)); break;       // d
                            default:
                                thePoints[2].push(vec4(x1,y1,z1,1.0)); break;       // c
                        } break;
                }
            }
        }

        // Create a nested for loop that goes through the columns and rows of the sphere (longitudinal)
        for(var j=0; j<iterations; j++) {
            for(var i=0; i<iterations + 1; i++) {
                // Determine theta, phi, and the xyz coordinates
                thet = i * 2 * Math.PI/iterations;
                phi = j * Math.PI/iterations;

                var x1 = radius * Math.cos(thet) * Math.sin(phi) + xtrans;
                var y1 = radius * Math.sin(thet) * Math.sin(phi) + ytrans;
                var z1 = radius * Math.cos(phi) + ztrans;

                // Push the point into the a, b, c, or d subarray depending on whether or not either of the
                // iterators is even or odd
                switch(j % 2 == 0) {
                    case(true):
                        switch(i % 2 == 0) {
                            case(true):
                                thePoints[0].push(vec4(x1,y1,z1,1.0)); break;       // a
                            default:
                                thePoints[1].push(vec4(x1,y1,z1,1.0)); break;       // b
                        } break;
                    default:
                        switch(i % 2 == 0) {
                            case(true):
                                thePoints[3].push(vec4(x1,y1,z1,1.0)); break;       // d
                            default:
                                thePoints[2].push(vec4(x1,y1,z1,1.0)); break;       // c
                        } break;
                }
            }
        }

        // Loop through all of the points, pushing four at a time for determining the two triangles
        for(var i = 0; i < iterations*90; i++) {
            indexPoints.push(thePoints[0][i]); // Vertex 0
            indexPoints.push(thePoints[1][i]); // Vertex 1
            indexPoints.push(thePoints[2][i]); // Vertex 2
            indexPoints.push(thePoints[3][i]); // Vertex 3
            pushPoints();
        }

        function pushPoints() {

            // Used to define the corners of the texture image
            var texCoord = [vec2(0,0), vec2(0,1), vec2(1,1), vec2(1,0)];

            // Get the normal
            var v1 = subtract(indexPoints[1], indexPoints[0]);
            var v2 = subtract(indexPoints[2], indexPoints[1]);
            var normal = cross(v1, v2);
            normal = vec3(normal);

            var indices = [ 0, 1, 2, 0, 2, 3 ];
            // Create the plane with two triangles and push each vertex to the points array.
            for ( var i = 0; i < indices.length; ++i ) {

                // Add to array each triangle
                points[texture].push( indexPoints[indices[i]] );
                normalsArray[texture].push(normal);

                // Color the two triangles white
                colors[texture].push([ 1.0, 1.0, 1.0, 1.0 ]);

                // Alter which texture coordinate to push into depending on i
                switch(i) {
                    case 0: texCoordsArray[texture].push(texCoord[0]); break;
                    case 1: texCoordsArray[texture].push(texCoord[1]); break;
                    case 2: texCoordsArray[texture].push(texCoord[2]); break;
                    case 3: texCoordsArray[texture].push(texCoord[0]); break;
                    case 4: texCoordsArray[texture].push(texCoord[2]); break;
                    default: texCoordsArray[texture].push(texCoord[3]); break;
                }
                pointsCount[texture] += 1; // Increment pointsCount for that texture
            }

            indexPoints = [];              // Reset indexPoints for the next four vertices
        }
    }
}

var polyCylinderTest = [
    vec4(0, 0, -5, 1.0),
    vec4(0, 0, 0, 1.0),
    vec4(0, 0, 5, 1.0)
]

class PolyCylinder {
    constructor(array,ballTexture,cylinderTexture,radius) {
        // Initialize variables
        var spheres = [];
        var cylinders = [];

        // Iterate through the length of the array that is passed in, plot the spheres
        for(var i = 0; i < array.length; i++) {
            spheres[i] = new Sphere(180,radius,ballTexture,array[i][0],array[i][1],array[i][2])
        }
        // Iterate through the length of the array that is passed in, plot the cylinders
        for(var i = 0; i < array.length - 1; i++) {
            cylinders[i] = new Cylinder((array[i+1][2] - array[i][2]),radius,cylinderTexture,array[i][0],array[i][1],array[i][2])
        }
    }
}

function LSystem(value, translation, value2, translation2, n) {
    var newTranslation = "";                                   // Initialize a string to represent the output
    for(var i = 0; i < n-1; i++) {                             // Loop through n - 1 many times
        for(var j = 0; j < translation.length; j++) {          // Loop through the length of the translation
            var char = translation.charAt(j);                  // Set variable char to the character at the position
            if(char == value) {                                // If the value in the parameter is the same as the character.
                newTranslation += translation;                 // Append the output by the translation
            }
            else if(char == value2) {                          // If the second value in the parameter is the same as the character.
                newTranslation += translation2;                // Append the output by the second translation
            }
            else { newTranslation += char }                    // Otherwise, just append the character
        }
    }
    treeSequence.push(newTranslation);                         // Push the LSystem sequence to an array for later use
}

function buildEverything() {
    //var test = new Cylinder(10,2,3,0,0,0,0,90,0);
    //var test1 = new Sphere(180,10,1,0,0,0);
    //var test2 = new PolyCylinder(polyCylinderTest,11,3,0.025);
    var terrain = new Terrain(100,11,-30,-10,0);
}

window.onload = function init() {
    
    // Get the canvas and shaders from the HTML
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    program  = initShaders( gl, "vertex-shader", "fragment-shader" );

    // Call the function for rendering each object
    buildEverything();

    // Specify canvas properties
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.725, 1.0, 0.78, 1.0 );
    gl.enable(gl.DEPTH_TEST);     

    //  Compile, Link shaders into program
    gl.useProgram( program );

    // Call these functions to create and specify the buffers
	loadVertices(program);
    loadColors(program);
    loadTextures(program);

    // Load the images for the textures
    for(var i = 0; i < imageTexture.length; i++) { loadImage(program, imageTexture[i], i); }

    // Get the location of the angle
    thetaLoc = gl.getUniformLocation(program, "theta");
    scaleLoc = gl.getUniformLocation(program, "scale");

    // Buttons
    document.getElementById( "xButton" ).onclick = function () { theta[xAxis] = 0; axis = xAxis; flag = true; };
    document.getElementById( "yButton" ).onclick = function () { theta[yAxis] = 0; axis = yAxis; flag = true; };
    document.getElementById( "zButton" ).onclick = function () { theta[zAxis] = 0; axis = zAxis; flag = true; };
    document.getElementById( "scaleUpButton" ).onclick = function () { scale += 0.0005; };
    document.getElementById( "scaleDownButton" ).onclick = function () { scale -= 0.0005; };
    document.getElementById( "freezeButton" ).onclick = function () { flag = !flag; };
    document.getElementById( "resetButton" ).onclick = function () {
        document.getElementById("fovySlider").value = 0;
        document.getElementById("xEyeSlider").value = 0;
        document.getElementById("yEyeSlider").value = 0;
        document.getElementById("zEyeSlider").value = 5;
        document.getElementById("xCtrSlider").value = 0;
        document.getElementById("yCtrSlider").value = 0;
        document.getElementById("zCtrSlider").value = -100;
        document.getElementById("xUpSlider").value = 0;
        document.getElementById("yUpSlider").value = 1;
        document.getElementById("zUpSlider").value = 1;
        document.getElementById("xSlider").value = 0;
        document.getElementById("ySlider").value = 0;
        document.getElementById("zSlider").value = 0;
        document.getElementById("xLightSlider").value = 0;
        document.getElementById("yLightSlider").value = 10;
        document.getElementById("zLightSlider").value = 200;
        fovy = 45;
        eyePt[0] = 0; eyePt[1] = 0; eyePt[2] = 5;
        atPt[0] = 0; atPt[1] = 0; atPt[2] = -100;
        upVec[0] = 0; upVec[1] = 1; upVec[2] = 0;
        theta[xAxis] = 0; theta[yAxis] = 0; theta[zAxis] = 0;
        lightPosition[0] = 0; lightPosition[1] = 10; lightPosition[2] = 200;
        flag = false;
    };

    // L System
    // Call the LSystem function for all of the sequences and display them in the HTML
    LSystem("F","F[+F]F[-F]F",0,0,2);
    LSystem("F","F[+F]F[-F][F]",0,0,2);
    LSystem("F","FF-[-F+F+F]+[+F-F-F]",0,0,2);
    LSystem("X","F[+X]F[-X]+X","F","FF",2);
    LSystem("X","F[+X][-X]FX","F","FF",2);
    LSystem("X","F-[[X]+X]+F[+FX]-X","F","FF",2);
    document.getElementById("treeSequenceA").innerHTML = "Tree Sequence A: " + treeSequence[0];
    document.getElementById("treeSequenceB").innerHTML = "Tree Sequence B: " + treeSequence[1];
    document.getElementById("treeSequenceC").innerHTML = "Tree Sequence C: " + treeSequence[2];
    document.getElementById("treeSequenceD").innerHTML = "Tree Sequence D: " + treeSequence[3];
    document.getElementById("treeSequenceE").innerHTML = "Tree Sequence E: " + treeSequence[4];
    document.getElementById("treeSequenceF").innerHTML = "Tree Sequence F: " + treeSequence[5];

    // Sliders
    document.getElementById("fovySlider").oninput = function(event) { fovy = event.target.value; };
    document.getElementById("xEyeSlider").oninput = function(event) { eyePt[0] = event.target.value; };
    document.getElementById("yEyeSlider").oninput = function(event) { eyePt[1] = event.target.value; };
    document.getElementById("zEyeSlider").oninput = function(event) { eyePt[2] = event.target.value; };
    document.getElementById("xCtrSlider").oninput = function(event) { atPt[0] = event.target.value; };
    document.getElementById("yCtrSlider").oninput = function(event) { atPt[1] = event.target.value; };
    document.getElementById("zCtrSlider").oninput = function(event) { atPt[2] = event.target.value; };
    document.getElementById("xUpSlider").oninput = function(event) { upVec[0] = event.target.value; };
    document.getElementById("yUpSlider").oninput = function(event) { upVec[1] = event.target.value; };
    document.getElementById("zUpSlider").oninput = function(event) { upVec[2] = event.target.value; };
    document.getElementById("xLightSlider").oninput = function(event) { lightPosition[0] = event.target.value; };
    document.getElementById("yLightSlider").oninput = function(event) { lightPosition[1] = event.target.value; };
    document.getElementById("zLightSlider").oninput = function(event) { lightPosition[2] = event.target.value; };
    document.getElementById("xSlider").oninput = function(event) { theta[xAxis] = event.target.value; };
    document.getElementById("ySlider").oninput = function(event) { theta[yAxis] = event.target.value; };
    document.getElementById("zSlider").oninput = function(event) { theta[zAxis] = event.target.value; };

    setViewProjection(program); // Call this to handle camera positioning using ViewMatrix and ProjectionMatrix
    render();                   // Render the scene
}

function loadVertices(program) {
    // Load normals for the diffuse light source
    var nBuffer = gl.createBuffer();                                             // Store normal vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedNormals = [].concat.apply([],normalsArray);                     // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedNormals), gl.STATIC_DRAW ); // Vertices stored in normalsArray.
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );                // Gets variable from vertex shader
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vs_Normal );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL

    var vBuffer = gl.createBuffer();                                             // Store vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );                                   // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedPoints = [].concat.apply([],points);                            // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedPoints), gl.STATIC_DRAW );  // Vertices stored in points
    
    // Load the vertices for the triangles and enable the attribute vPosition
    var vPosition = gl.getAttribLocation( program, "vPosition" );                // Gets variable from vertex shader 
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );               // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vPosition );                                     // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadColors(program) {
    // Store colors in a buffer
    var cBuffer = gl.createBuffer();                                            // Store color vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedColors = [].concat.apply([],colors);                           // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(flattenedColors), gl.STATIC_DRAW ); // Vertices stored in colors

    var vColor = gl.getAttribLocation( program, "vColor" );                     // Load the colors for the triangles and enable the attribute vColor
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );                 // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vColor );                                       // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadTextures(program) {
    var tBuffer = gl.createBuffer();                                          // Store texture vertices in a buffer for the shaders to use
    gl.bindBuffer(gl.ARRAY_BUFFER, tBuffer);                                  // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    var flattenedTextures = [].concat.apply([],texCoordsArray);               // Flatten the 2D array of vertices (since the subarrays are for each texture)
    gl.bufferData(gl.ARRAY_BUFFER,flatten(flattenedTextures),gl.STATIC_DRAW); // Vertices stored in textures

    var vTexCoord = gl.getAttribLocation(program, "vTexCoord");               // Load the textures for the triangles and enable the attribute vTexCoord
    gl.vertexAttribPointer(vTexCoord,2,gl.FLOAT,false,0,0);                   // Specifies organization of data in the arrays
    gl.enableVertexAttribArray(vTexCoord);                                    // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadImage(program, url, txtUnit) {
    var texture = gl.createTexture();                                       // Create a texture object
    fs_textureLoc = gl.getUniformLocation(program, "fs_texture");           // Get the location of the fragment shader texture parameter
    var image = new Image();                                                // Create an image object
    image.onload = function() { loadTexture(image, texture, txtUnit); };    // Set up an event handler that goes off when an image is finished loading
    image.crossOrigin = "anonymous";                                        // Make it so the CORS test passes
    image.src = url;                                                        // Load image from URL
}

function loadTexture(image, texture, txtUnit) {
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);                              // Flip the Y-coordinates
    gl.activeTexture( gl.TEXTURE0+txtUnit );                                // Assign the active texture to internal texture unit 0 for rendering
    gl.bindTexture(gl.TEXTURE_2D, texture);                                 // Bind the texture
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.CLAMP_TO_EDGE);     // Assume the image is of a power of 2
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,gl.LINEAR);       // Assume the image is of a power of 2
    gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,image);  // Create the 2D image from the texture
    imageLoadedCount += 1;                                                  // Increment the imageLoadedCount
}

function setViewProjection( program ){
    VMatrixLoc = gl.getUniformLocation( program, "vs_ViewMatrix" );          // Gets the location of the view matrix
    PMatrixLoc = gl.getUniformLocation( program, "vs_ProjMatrix" );          // Gets the location of the projection matrix

    // Return error message if the location cannot be found
    if(!VMatrixLoc || !PMatrixLoc) { console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix'); return; }

    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                              // Set the view matrix depending on the camera coordinates
    aspect = canvas.width/canvas.height;                                     // Set the aspect to ratio of canvas width and height
    vs_ProjMatrix = perspective(fovy, aspect, near, far);                    // Set the projection matrix depending on the camera coordinates

    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix));         // Set up the uniform matrix for the view matrix
    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix));         // Set up the uniform matrix for the projection matrix
}

function loadPhongModel() {
    var ambientProduct = mult(lightAmbient, materialAmbient);                // Calculate the ambientProduct
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);                // Calculate the diffuseProduct
    var specularProduct = mult(lightSpecular, materialSpecular);             // Calculate the specularProduct

    gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"), flatten(ambientProduct) );   // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"), flatten(diffuseProduct) );   // Set up the uniform matrix for the diffuse
    gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"), flatten(specularProduct) ); // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"), flatten(lightPosition) );     // Set up the uniform matrix for the light position
    gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"), materialShininess);                // Set up the uniform matrix for the shininess
}

// This function creates the dynamic values displayed on the UI as the sliders are being used
function updateValuesDisplay() 
{
    var el = document.querySelector('#eyeText');
    el.innerHTML = '<strong>Eye ( '+ eyePt[0] + ', ' + eyePt[1] + ', ' + eyePt[2] + ' )</strong>';

    el = document.querySelector('#centerText');
    el.innerHTML = '<strong>Center ( '+ atPt[0] + ', ' + atPt[1] + ', ' + atPt[2] + ' )</strong>';

    el = document.querySelector('#upText');
    el.innerHTML = '<strong>Up ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[1] + ' )</strong>';

    el = document.querySelector('#fovyText');
    el.innerHTML = '<strong>Field of View ( '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[2] + ' )</strong>';

    var el = document.querySelector('#lightText');
    el.innerHTML = '<strong>Light ( '+ lightPosition[0] + ', ' + lightPosition[1] + ', ' + lightPosition[2] + ' )</strong>';

    var el = document.querySelector('#xSliderText');
    el.innerHTML = '<strong>X-Direction ( '+ theta[xAxis] + ' )</strong>';

    var el = document.querySelector('#ySliderText');
    el.innerHTML = '<strong>Y-Direction ( '+ theta[yAxis] + ' )</strong>';

    var el = document.querySelector('#zSliderText');
    el.innerHTML = '<strong>Z-Direction ( '+ theta[zAxis] + ' )</strong>';
}

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );                // Create the window on the screen
    updateValuesDisplay();                                                // Call this to update UI

    if(flag) theta[axis] += 1.0;                                          // If a rotate or freeze button has been pressed, react accordingly
    gl.uniform3fv(thetaLoc, theta);                                       // Account for the location of theta
    gl.uniform1f(scaleLoc, scale);                                        // Account for the location of scale

    vs_ProjMatrix = perspective(fovy, aspect, near, far);                 // Set the position of the projection matrix
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                           // Set the position of the view matrix

    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0])); // Handle rotation in the x-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0])); // Handle rotation in the y-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1])); // Handle rotation in the z-direction

    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix) );     // Set up the uniform matrix for the projection matrix
    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix) );     // Set up the uniform matrix for the view matrix
    loadPhongModel();                                                     // Call loadPhongModel()

    if(imageLoadedCount == imageTexture.length) {                         // If all of the images have been loaded
    
        var objOffset = 0;                                                // Initialize the offset value
        for (var txtUnit = 0; txtUnit < imageTexture.length; txtUnit++) { // Loop through the number of faces
        
            gl.uniform1i(fs_textureLoc, txtUnit);                         // Reference the location of the fragment shader for the texture
            gl.drawArrays(gl.TRIANGLES, objOffset, pointsCount[txtUnit]); // Draw out all of the textures
            objOffset += pointsCount[txtUnit];                            // Increment the offset
        }
    }
    requestAnimFrame( render );                                           // Call the render function recursively for each frame
}