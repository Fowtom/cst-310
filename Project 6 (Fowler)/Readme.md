Thomas Fowler
CST-310, MWF155A
Jeff Griffith
11 March 2018

PROJECT 6: FRACTAL ART

In this project, I incorporated L-systems, polycylinders, and a terrain mesh. This project serves as the foundation for the Project #7, in which the polycylinders will be dependent on the L-system strings and values.

DIRECTIONS TO RENDER THE SCENE
1) Ensure that terrain.html and terrain.js are both in the same directory, and the folder that contains both of these is in the same directory as the "Common" folder with all of the extra javascript files.
2) Open "terrain.html"
3) If executed correctly, the scene interface should appear within your default web browser.

