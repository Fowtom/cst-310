// Main scene objects
var container, camera, scene, renderer;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

// Arrays for holding the data
var positions = [];
var currentPosition = [];
var nextPosition = [];
var cylinders = [];

// Icosahedron
var icoGeometry = new THREE.IcosahedronGeometry(18,0);
changeColors();
var icoMaterial = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );
var ico = new THREE.Mesh( icoGeometry, icoMaterial );

// Cylinders
var cylGeometry = new THREE.CylinderGeometry( 20, 40, 200, 20 );
var cylMaterial = new THREE.MeshPhongMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );

// Linear interpolation functions
// Inspired by: https://stackoverflow.com/questions/47733935/threejs-move-object-from-point-a-to-point-b
// http://paulbourke.net/miscellaneous/interpolation/
function cerp(firstPoint, secondPoint, t) {
    var t2;
    t2 = (1-Math.cos(t*Math.PI))/2;
    return (firstPoint*(1-t2)+secondPoint*t2); }
function ease(t) { return t < 0.5 ? 2*t*t : -1+(4-2*t)*t}
var t = 0, dt = 0.01;

// Used for epileptic icosahedron
function changeColors() {
    for ( var i = 0; i < icoGeometry.faces.length; i += 2 ) {
        var hex = Math.random() * 0xffffff;
        icoGeometry.faces[ i ].color.setHex( hex );
        icoGeometry.faces[ i + 1 ].color.setHex( hex );
    }
}

// Used to rotate all of the cylinders
function rotateCylinders() { for(var i = 0; i < 16; i++) { cylinders[i].rotation.y += 5; } }

// This will add a starfield to the background of a scene
// Inspired by https://threejs.org/docs/#api/materials/PointsMaterial
var starGeometry = new THREE.Geometry();
for ( var i = 0; i < 5000; i ++ ) {
    var star = new THREE.Vector3();
    star.x = THREE.Math.randFloatSpread( 2500 );
    star.y = THREE.Math.randFloatSpread( 2500 );
    star.z = THREE.Math.randFloatSpread( 2500 );
    starGeometry.vertices.push( star );
}
var starMaterial = new THREE.PointsMaterial( { color: 0xFFFFFF } );
var stars = new THREE.Points( starGeometry, starMaterial );

// Begin the program
init();
animate();

function init() {
    // Initialize the container
    container = document.createElement( 'div' );
    document.body.appendChild( container );
    
    // Initialize Camera
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000 );
    camera.position.y = 200;
    camera.position.z = 500;
    
    // Initialize Scene
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x000000 );
    scene.add( stars );

    // White directional light at half intensity shining from the top.
    var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
    light.castShadow = true;
    light.position.y = 300;
    light.position.z = 400;
    scene.add( light );

    // Cylinder
    var xDistance = 100;
    var zDistance = 100;
    var xOffset = -310;
    
    // Loop through all of the cylinders
    for(var i = 0; i < 4; i++) {
        for(var j = 0; j < 4; j++) {
            // Create the cylinder
            var cyl  = new THREE.Mesh(cylGeometry, cylMaterial);
            // Assign its color
            var hex = Math.random() * 0xffffff;
            cyl.material.color.setHex(hex);
            // Position it
            cyl.position.x = (xDistance * i) + xOffset;
            cyl.position.y = 75+30*i;
            cyl.position.z = (zDistance * j);
            // Push the positions to the positions array
            positions.push(vec3(cyl.position.x,cyl.position.y+135,cyl.position.z));
            cylinders.push(cyl);
            // Add it to the scene
            scene.add(cyl);
        }
    };

    // Assign where the icosahedron will start
    currentPosition[0] = positions[3][0];
    currentPosition[1] = positions[3][1];
    currentPosition[2] = positions[3][2];
    ico.position.x = currentPosition[0];
    ico.position.y = currentPosition[1];
    ico.position.z = currentPosition[2];
    
    // Initialize where the icosahedron will go next
    var seed = Math.floor(Math.random()*16);
    nextPosition[0] = positions[seed][0];
    nextPosition[1] = positions[seed][1];
    nextPosition[2] = positions[seed][2];
    
    // Add the icosahedron to the scene
    scene.add(ico);
    
    // Rotate the scene for a better view
    scene.rotateY(189.3);
    
    // Initialize the renderer and add preferences
    renderer = new THREE.CanvasRenderer();
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );
    
    // Call this function so the icosahedron moves around
    loop();

    function loop() {
        // Call linear interpolation and ease functions to get position of icosahedron while it is moving around
        var nextX = cerp(currentPosition[0], nextPosition[0], ease(t));   // Interpolate between first position and second position where
        var nextY = cerp(currentPosition[1], nextPosition[1], ease(t));   // t is first passed through an easing function
        var nextZ = cerp(currentPosition[2], nextPosition[2], ease(t));   
        ico.position.set(nextX, nextY, nextZ);  // Set the icosahedron's new position
        
        // If the icosahedron has reached its next position
        if(Math.ceil(nextX) == nextPosition[0] && Math.ceil(nextY) == nextPosition[1] && Math.ceil(nextZ) == nextPosition[2]) {
            // Save the current position to the previous next position, and assign a new next position
            var seed = Math.floor(Math.random()*16);
            currentPosition[0] = nextPosition[0];
            currentPosition[1] = nextPosition[1];
            currentPosition[2] = nextPosition[2];
            nextPosition[0] = positions[seed][0];
            nextPosition[1] = positions[seed][1];
            nextPosition[2] = positions[seed][2];
            // Reset t
            t = 0.02;
        }
        // Increment t by dt
        t += dt;
        // Check if t is outside of its range to control speed
        if (t <= 0 || t >=1) dt = -dt;
        // Update the renderer and request the animation frame for the loop
        renderer.render(scene, camera);
        requestAnimationFrame(loop);
    }
    // Used to ensure the scene is fully visible even when not in full screen
    window.addEventListener( 'resize', onWindowResize, false );
}

// Used to ensure the scene is fully visible even when not in full screen
function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() { requestAnimationFrame( animate ); render(); }

function render() {
    // Consistently rotate the icosahedron, and change its colors while it is moving
    ico.rotation.x += 0.10;
    ico.rotation.y += 0.02;
    ico.rotation.z += 0.01;
    // Update the position of the cylinders and change the color of the icosahedron
    rotateCylinders();
    changeColors();
    renderer.render( scene, camera );
}