Thomas Fowler
CST-310, MWF155A
Jeff Griffith
8 April 2018

PROJECT 8: DEMO SCENE

In this project, I created a demo scene using many of the concepts I have previously learned, as well as some new ones using three.js.

DIRECTIONS TO RUN THE DEMO
1) Ensure that project8.html and project8.js are both in the same directory, and the folder that contains both of these is in the same directory as the "Common" folder with all of the extra JavaScript files.
2) Open "project8.html"
3) If executed correctly, the demo scene should appear.