Thomas Fowler
CST-310, MWF155A
Jeff Griffith
25 February 2018

PROJECT 4: RENDER YOUR SCENE WITH PRIMITIVES

In this project, I took the scene I chose from Projects 2 and 3 and implemented it using WebGL, HTML, and JavaScript. It supports many different transformations, such as scaling, rotation, translation, zoom, freeze, light position, and reset. See the "Project 4: Render your Scene with Primitives" section of "Your Surrounding World (Fowler).docx" for further details.

DIRECTIONS TO RENDER THE SCENE
1) Ensure that the two folders, Scene and Common, are both in the same directory.
2) Within the Scene folder, open "scene.html"
3) If executed correctly, the scene interface should appear within your default web browser.

