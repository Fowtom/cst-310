"use strict";

// Initialize canvas and gl variables
var canvas;
var gl;

// Used to store the data
var points = [];
var normalsArray = [];
var colors = [];

// Used for rotation and scaling
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];
var thetaLoc;
var scale = 0.01;
var scaleLoc;

// Used for camera positioning
var near = 1;
var far = 100;
var fovy = 45.0;
var aspect;
var flag = false;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;
var eyePt = vec3(0, 0, 5);
const atPt = vec3(0.0, 0.0, -100);
const upVec = vec3(0.0, 1.0, 0.0);

// These variables handle the light aspects
var lightPosition = vec4(0.0, 10.5, 200.0, 0.0 );
var materialShininess = 100.0;
var lightAmbient = vec4(0.1, 0.1, 0.1, 1.0 );
var lightDiffuse = vec4( .5, .5, .5, 1.0 );
var lightSpecular = vec4( .5, .5, .5, 1.0 );
var materialAmbient = vec4( .1, .1, .1, 1.0 );
var materialDiffuse = vec4( 1, 1, 1, 1.0);
var materialSpecular = vec4( .1, .1, .1, 1.0 );

// Used for loading the shaders
var program;

// All of these variables are used in the following functions that are called.
var wallFloorScale = 100;
var cornerScale = 3;
var cornerHeight = 50;
var beamLength = 28;
var beamHeight = 4;
var beamDepth = 1.5;
var metalFramePieceLength = 30;
var metalFramePieceHeight = 3;
var metalFramePieceDepth = 66;
var supportLatchScale = 1.1;
var supportLatchHeight = 30;
var connectingLatchLength = 5;
var connectingLatchHeight = 30;
var connectingLatchDepth = 5;
var fridgeScale = 18;
var fridgeHeight = 30;
var fridgeDoorWidth = 3;
var wallStripHeight = 4;
var wallSegmentSideLength = 25;
var wallTopLength = 40;
var wallTopHeight = 25;
var wallBottomHeight = 35;
var windowPaneBaseLength = 40;
var windowPaneSideLength = 60;
var windowPaneHeight = 40;
var windowPaneDepth = 9;
var blindLength = 3;
var blindHeight = 38.5;
var blindDepth = 1;
var extrusionScale = 25;
var outletLength = 1;
var outletHeight = 5;
var outletDepth = 3;
var bedLength = 24;
var bedHeight = 60;
var bedDepth = 58;
var bedSideLength = 4;
var bedSideHeight = 4;
var bedSideDepth = 56;

class Plane {

    // Whenever a new plane object is built, each of these parameters specify its size, color, and location.
    constructor(length,height,depth,a,b,c,d,color,xtrans,ytrans,ztrans) {

        // Change vertex values by the parameter specifications
        var vertices = [
            vec4( -length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 0
            vec4( -length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 1
            vec4(  length+xtrans,  height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 2
            vec4(  length+xtrans, -height+ytrans,  depth+ztrans, 1.0 ),  // Vertex 3
            vec4( -length+xtrans, -height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 4
            vec4( -length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 5
            vec4(  length+xtrans,  height+ytrans, -depth+ztrans, 1.0 ),  // Vertex 6
            vec4(  length+xtrans, -height+ytrans, -depth+ztrans, 1.0 )   // Vertex 7
        ];
        
        // Change color by parameter specifications
        var faceColors = [
            [ 0.68, 0.68, 0.68, 1.0 ],  // gray
            [ 0.87, 0.87, 0.87, 1.0 ],  // white
            [ 0.76, 0.58, 0.22, 1.0 ],  // brown
            [ 0.27, 0.29, 0.35, 1.0 ],  // navy gray
            [ 0.0, 0.0, 0.0, 1.0 ],      // black
            [ 0.25, 0.27, 0.33, 1.0 ]  // wall strip navy gray
        ];

        // Get the normal
        var v1 = subtract(vertices[b], vertices[a]);
        var v2 = subtract(vertices[c], vertices[b]);
        var normal = cross(v1, v2);
        normal = vec3(normal);

        // Create the plane with two triangles and push each vertex to the points array.
        var indices = [ a, b, c, a, c, d ];
        for ( var i = 0; i < indices.length; ++i ) 
        {
            // Add to array each triangle
            points.push( vertices[indices[i]] );
            normalsArray.push(normal);

            // Color the two triangles the same color
            colors.push(faceColors[color]);
        }
    }
}

class Prism {

    // This class essentially creates six plane objects, one for each face of the prism.
    constructor(length,height,depth,color,xtrans,ytrans,ztrans){
        var base = new Plane(length,height,depth,3,0,4,7,color,xtrans,ytrans,ztrans);
        var top = new Plane(length,height,depth,6,5,1,2,color,xtrans,ytrans,ztrans);
        var front = new Plane(length,height,depth,4,5,6,7,color,xtrans,ytrans,ztrans);
        var left = new Plane(length,height,depth,5,4,0,1,color,xtrans,ytrans,ztrans);
        var back = new Plane(length,height,depth,1,2,3,0,color,xtrans,ytrans,ztrans);
        var right = new Plane(length,height,depth,2,3,7,6,color,xtrans,ytrans,ztrans);
    }
}

function buildEverything() {

    // Call each of the functions
    buildWallsAndFloor();
    buildBedFrame();
    buildBed();
    buildFridge();
    buildWallBaseStrips();
    buildWindow();
    buildWallOutlet();
}

function buildWallsAndFloor() {

    // Build the floor, ceiling, and left wall
    var floor = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,3,0,4,7,3,0,-0.01,0);
    var ceiling = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,1,2,6,5,1,0,0,0);
    var leftWall = new Plane(wallFloorScale,wallFloorScale,wallFloorScale,5,4,0,1,1,0,0,0);

    // These segments allow room for the window pane in the center of the right wall
    var rightWallLeftSegment = new Plane(wallSegmentSideLength,wallFloorScale,wallFloorScale,1,2,3,0,1,-75,0,0);
    var rightWallRightSegment = new Plane(wallSegmentSideLength,wallFloorScale,wallFloorScale,1,2,3,0,1,55,0,0);
    var rightWallTopSegment = new Plane(wallTopLength,wallTopHeight,wallFloorScale,1,2,3,0,1,-10,75,0);
    var rightWallBottomSegment = new Plane(wallTopLength,wallBottomHeight,wallFloorScale,1,2,3,0,1,-10,-65,0);

    // Though not included originally, there is an extrusion in the wall of my bedroom
    var extrusion = new Prism(extrusionScale,wallFloorScale,extrusionScale,1,75,0,75);
}

function buildBedFrame() {

    // Build the corner pieces of the bed frame
    var topLeftCorner = new Prism(cornerScale,cornerHeight,cornerScale,2,-96,-50,96);
    var topRightCorner = new Prism(cornerScale,cornerHeight,cornerScale,2,-36,-50,96);
    var bottomLeftCorner = new Prism(cornerScale,cornerHeight,cornerScale,2,-96,-50,-40);
    var bottomRightCorner = new Prism(cornerScale,cornerHeight,cornerScale,2,-36,-50,-40);

    // Build the connecting beams
    var connectingBeams = [];
    for(var i = 0; i < 10; i++)
    {
        if(i < 5) { connectingBeams[i] = new Prism(beamLength,beamHeight,beamDepth,2,-66,-90+20*i,94); }
        else { connectingBeams[i] = new Prism(beamLength,beamHeight,beamDepth,2,-66,-90+20*(i-5),-42); }
    }

    // Build the metal pieces for supporting the bed
    var metalFramePiece = new Prism(metalFramePieceLength,metalFramePieceHeight,metalFramePieceDepth,4,-66,-52,27);
    var supportLatch1 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,4,-96,-30,94);
    var supportLatch2 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,4,-36,-30,94);
    var supportLatch3 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,4,-96,-30,-38);
    var supportLatch4 = new Prism(supportLatchScale,supportLatchHeight,supportLatchScale,4,-36,-30,-38);
}

function buildBed() {

    // Instead of building a rectangular prism to represent the bed, I decided to 
    // have edges and corner pieces to define the dimensions of the bed. The following
    // objects represent each of the bed faces.
    var bedTop = new Plane(bedLength,bedHeight,bedDepth,1,2,6,5,4,-67,-89,26);
    var bedBottom = new Plane(bedLength,bedHeight,bedDepth,1,2,6,5,4,-67,-110,26);
    var bedBack = new Plane(bedLength,bedSideHeight,bedDepth,1,2,3,0,4,-67,-38,32);
    var bedFront = new Plane(bedLength,bedSideHeight,bedDepth,4,5,6,7,4,-67,-38,21);

    var bedRightSide = new Plane(bedSideLength,bedSideHeight+1,bedSideDepth,6,2,3,7,4,-40,-40,26);
    var bedLeftSide = new Plane(bedSideLength,bedSideHeight+1,bedSideDepth,5,1,0,4,4,-95,-40,26);
    var bedTopRightSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,1,5,7,3,4,-40,-32,26);
    var bedTopLeftSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,6,2,0,4,4,-95,-32,26);
    var bedBottomRightSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,6,2,0,4,4,-40,-47,26);
    var bedBottomLeftSide = new Plane(bedSideLength,bedSideHeight-1,bedSideDepth,1,5,7,3,4,-95,-47,26);
    
    var bedTopFrontSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,4,1,2,7,4,-67,-32,-33);
    var bedTopBackSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,5,0,3,6,4,-67,-32,87);
    var bedBottomFrontSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,5,0,3,6,4,-67,-45,-33);
    var bedBottomBackSide = new Plane(bedLength,bedSideHeight-1,bedSideLength,1,2,7,4,4,-67,-45,87);

    var cornerPieceBR = new Plane(bedSideLength,bedSideLength,bedSideLength,4,5,2,3,4,-40,-40,-32);
    var cornerPieceBL = new Plane(bedSideLength,bedSideLength,bedSideLength,1,0,7,6,4,-95,-40,-32);
    var cornerPieceTR = new Plane(bedSideLength,bedSideLength,bedSideLength,1,0,7,6,4,-40,-40,87);
    var cornerPieceTL = new Plane(bedSideLength,bedSideLength,bedSideLength,4,5,2,3,4,-95,-40,87);
}

function buildFridge() {

    // The fridge is simply two rectangular prisms adjacent to each other.
    var miniFridge = new Prism(fridgeScale,fridgeHeight,fridgeScale,4,-10,-70,78);
    var miniFridgeDoor = new Prism(fridgeScale,fridgeHeight,fridgeDoorWidth,0,-10,-70,56);
}

function buildWallBaseStrips() {

    // The base strips run along the edge of the walls, making contact with the floor
    var leftWallBaseStrip = new Plane(wallFloorScale,wallStripHeight,wallFloorScale,5,4,0,1,5,1,-96,0);
    var rightWallBaseStrip = new Plane(wallFloorScale,wallStripHeight,wallFloorScale,1,2,3,0,5,0,-96,-1);
    var leftExtrusionBaseStrip = new Plane(extrusionScale,wallStripHeight,extrusionScale,5,4,0,1,5,74,-96,74);
    var frontExtrusionBaseStrip = new Plane(extrusionScale+1,wallStripHeight,extrusionScale,1,2,3,0,5,74,-96,24);
}

function buildWindow() {

    // The window pane is from the panel that was cut out of the right wall earlier
    var windowPaneBase = new Plane(windowPaneBaseLength,windowPaneHeight,windowPaneDepth,3,0,4,7,1,-10,10,109);
    var windowPaneLeft = new Plane(windowPaneSideLength,windowPaneHeight,windowPaneDepth,5,4,0,1,1,10,10,109);
    var windowPaneTop = new Plane(windowPaneBaseLength,windowPaneHeight,windowPaneDepth,1,2,6,5,1,-10,10,109);
    var windowPaneRight = new Plane(windowPaneSideLength,windowPaneHeight,windowPaneDepth,2,3,7,6,1,-30,10,109);

    // Build all of the blinds
    var blindSupport = new Prism(39,4,5,0,-10,45,105);
    var blinds = [];
    for(var i = 0; i < 11; i++) { blinds[i] = new Prism(blindLength,blindHeight,blindDepth,0,-45+7*i,10.5,105); }
}

function buildWallOutlet() {

    // The wall outlet is a rectangular prism with two squares to represent the holes
    var outlet = new Prism(outletLength,outletHeight,outletDepth,0,-98,-65,-65);
    var holes1 = new Plane(1,1,1,5,4,0,1,4,-95.9,-63,-65);
    var holes2 = new Plane(1,1,1,5,4,0,1,4,-95.9,-67,-65);
}

window.onload = function init() {
    
    // Get the canvas and shaders from the HTML
    canvas = document.getElementById( "gl-canvas" );
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    program  = initShaders( gl, "vertex-shader", "fragment-shader" );

    // Call the function for rendering each object
    buildEverything();

    // Specify canvas properties
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.725, 1.0, 0.78, 1.0 );
    gl.enable(gl.DEPTH_TEST);     

    //  Compile, Link shaders into program
    gl.useProgram( program );

    // Call these functions to create and specify the buffers
	loadVertices(program);
    loadColors(program);

    // Get the location of the angle
    thetaLoc = gl.getUniformLocation(program, "theta");
    scaleLoc = gl.getUniformLocation(program, "scale");

    // Buttons
    document.getElementById( "xButton" ).onclick = function () { axis = xAxis; flag = true; };
    document.getElementById( "yButton" ).onclick = function () { axis = yAxis; flag = true; };
    document.getElementById( "zButton" ).onclick = function () { axis = zAxis; flag = true; };
    document.getElementById( "scaleUpButton" ).onclick = function () { scale += 0.005; };
    document.getElementById( "scaleDownButton" ).onclick = function () { scale -= 0.005; };
    document.getElementById( "freezeButton" ).onclick = function () { flag = !flag; };
    document.getElementById( "resetButton" ).onclick = function () {
        document.getElementById("fovySlider").value = 0;
        document.getElementById("xEyeSlider").value = 0;
        document.getElementById("yEyeSlider").value = 0;
        document.getElementById("zEyeSlider").value = 0;
        document.getElementById("xCtrSlider").value = 0;
        document.getElementById("yCtrSlider").value = 0;
        document.getElementById("zCtrSlider").value = 0;
        document.getElementById("xUpSlider").value = 0;
        document.getElementById("yUpSlider").value = 0;
        document.getElementById("zUpSlider").value = 0;
        fovy = 45;
        eyePt[0] = 0; eyePt[1] = 0; eyePt[2] = 5;
        atPt[0] = 0; atPt[1] = 0; atPt[2] = -100;
        upVec[0] = 0; upVec[1] = 1; upVec[2] = 0;
    };

    // Sliders
    document.getElementById("fovySlider").onchange = function(event) { fovy = event.target.value; };
    document.getElementById("xEyeSlider").onchange = function(event) { eyePt[0] = event.target.value; };
    document.getElementById("yEyeSlider").onchange = function(event) { eyePt[1] = event.target.value; };
    document.getElementById("zEyeSlider").onchange = function(event) { eyePt[2] = event.target.value; };
    document.getElementById("xCtrSlider").onchange = function(event) { atPt[0] = event.target.value; };
    document.getElementById("yCtrSlider").onchange = function(event) { atPt[1] = event.target.value; };
    document.getElementById("zCtrSlider").onchange = function(event) { atPt[2] = event.target.value; };
    document.getElementById("xUpSlider").onchange = function(event) { lightPosition[0] = event.target.value; };
    document.getElementById("yUpSlider").onchange = function(event) { lightPosition[1] = event.target.value; };
    document.getElementById("zUpSlider").onchange = function(event) { lightPosition[2] = event.target.value; };

    setViewProjection(program); // Call this to handle camera positioning using ViewMatrix and ProjectionMatrix
    render();                   // Render the scene
}

function loadVertices(program) {

    // Load normals for the diffuse light source
    var nBuffer = gl.createBuffer();                                         // Store normal vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normalsArray), gl.STATIC_DRAW ); // Vertices stored in normalsArray.
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );            // Gets variable from vertex shader
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );           // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vs_Normal );                                 // Enables the vertex attribute by copying the buffer to the VBO of WebGL

    var vBuffer = gl.createBuffer();                                         // Store vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );       // Vertices stored in points
    
    // Load the vertices for the triangles and enable the attribute vPosition
    var vPosition = gl.getAttribLocation( program, "vPosition" );            // Gets variable from vertex shader 
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );           // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vPosition );                                 // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function loadColors(program) {

    // Store colors in a buffer
    var cBuffer = gl.createBuffer();                                         // Store color vertices in a buffer for the shaders to use
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );                               // gl.ARRAY_BUFFER says the buffer is of type vertex attributes
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );       // Vertices stored in colors

    var vColor = gl.getAttribLocation( program, "vColor" );                  // Load the colors for the triangles and enable the attribute vColor
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );              // Specifies organization of data in the arrays
    gl.enableVertexAttribArray( vColor );                                    // Enables the vertex attribute by copying the buffer to the VBO of WebGL
}

function setViewProjection( program ){

    VMatrixLoc = gl.getUniformLocation( program, "vs_ViewMatrix" );          // Gets the location of the view matrix
    PMatrixLoc = gl.getUniformLocation( program, "vs_ProjMatrix" );          // Gets the location of the projection matrix

    // Return error message if the location cannot be found
    if(!VMatrixLoc || !PMatrixLoc) { console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix'); return; }

    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                              // Set the view matrix depending on the camera coordinates
    aspect = canvas.width/canvas.height;                                     // Set the aspect to ratio of canvas width and height
    vs_ProjMatrix = perspective(fovy, aspect, near, far);                    // Set the projection matrix depending on the camera coordinates

    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix));         // Set up the uniform matrix for the view matrix
    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix));         // Set up the uniform matrix for the projection matrix
}

function loadPhongModel() {
    var ambientProduct = mult(lightAmbient, materialAmbient);                // Calculate the ambientProduct
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);                // Calculate the diffuseProduct
    var specularProduct = mult(lightSpecular, materialSpecular);             // Calculate the specularProduct

    gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"), flatten(ambientProduct) );   // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"), flatten(diffuseProduct) );   // Set up the uniform matrix for the diffuse
    gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"), flatten(specularProduct) ); // Set up the uniform matrix for the ambient
    gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"), flatten(lightPosition) );     // Set up the uniform matrix for the light position
    gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"), materialShininess);                // Set up the uniform matrix for the shininess
}

// This function creates the dynamic values displayed on the UI as the sliders are being used
function updateValuesDisplay() 
{
    var el = document.querySelector('#eyeText');
    el.innerHTML = '<strong>eye {'+ eyePt[0] + ', ' + eyePt[1] + ', ' + eyePt[2] + '}</strong>';

    el = document.querySelector('#centerText');
    el.innerHTML = '<strong>center {'+ atPt[0] + ', ' + atPt[1] + ', ' + atPt[2] + '}</strong>';

    el = document.querySelector('#upText');
    el.innerHTML = '<strong>up &lt'+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[1] + '&gt;</strong>';

    el = document.querySelector('#fovyText');
    el.innerHTML = '<strong>Field of View { '+ upVec[0] + ', ' + upVec[1] + ', ' + upVec[2] + ' }</strong>';
}

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );                // Create the window on the screen
    updateValuesDisplay();                                                // Call this to update UI

    if(flag) theta[axis] += 1.0;                                          // If a rotate or freeze button has been pressed, react accordingly
    gl.uniform3fv(thetaLoc, theta);                                       // Account for the location of theta
    gl.uniform1f(scaleLoc, scale);                                        // Account for the location of scale

    vs_ProjMatrix = perspective(fovy, aspect, near, far);                 // Set the position of the projection matrix
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);                           // Set the position of the view matrix

    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0])); // Handle rotation in the x-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0])); // Handle rotation in the y-direction
    vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1])); // Handle rotation in the z-direction

    gl.uniformMatrix4fv( PMatrixLoc, false, flatten(vs_ProjMatrix) );     // Set up the uniform matrix for the projection matrix
    gl.uniformMatrix4fv( VMatrixLoc, false, flatten(vs_ViewMatrix) );     // Set up the uniform matrix for the view matrix
    loadPhongModel();                                                     // Call loadPhongModel()
    gl.drawArrays( gl.TRIANGLES, 0, points.length );                      // Use gl.TRIANGLES to draw everything

    requestAnimFrame( render );                                           // Call the render function recursively for each frame
}